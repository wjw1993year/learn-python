from collections import defaultdict
from math import inf
from typing import List


class LeetCode5897:
    def minimumDifference(self, nums: List[int]) -> int:
        s = sum(nums)
        target = s // 2
        n = len(nums) // 2
        # 等长切分数组
        a = nums[:n]
        b = nums[n:]

        dpa = defaultdict(set)
        dpb = defaultdict(set)
        dpa[0].add(0)
        dpb[0].add(0)
        # 状态压缩计算每个长度对应的不同计算结果
        for i in range(1 << n):
            cnt = bin(i).count('1')
            sa = 0
            sb = 0
            for j in range(n):
                if i >> j & 1:
                    sa += a[j]
                    sb += b[j]
            dpa[cnt].add(sa)
            dpb[cnt].add(sb)

        res = inf
        # 双指针
        for i in range(n + 1):
            # 防止一边为0，需要补0，排序后二分查找/双指针查找
            aa = sorted(list(dpa[i]))
            ab = sorted(list(dpb[n - i]))
            i, j = 0, len(ab) - 1
            while i < len(aa) and j >= 0:
                if aa[i] + ab[j] == target:
                    return abs(target * 2 - s)
                elif i + 1 < len(aa) and aa[i] + ab[j] < target:
                    i += 1
                else:
                    j -= 1
                res = min(res, abs(s - 2 * (aa[i] + ab[j])))
        return res


if __name__ == '__main__':
    nums = [3, 9, 7, 2]
    solution = LeetCode5897()
    print(solution.minimumDifference(nums))
