"""
只要pyi中定义了签名，编译器就不会报错，但是实际运行时会报错
"""

import learnpy.pagkage as pag

if __name__ == '__main__':
    pag.m1()  # m1()方法在pyi中定义了，所以编译器不报错。但是它没有实现，所以不能运行
