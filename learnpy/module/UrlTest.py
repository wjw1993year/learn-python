"""
urllib模块
"""

from urllib import request


# GET方法
def test_get():
    with request.urlopen("https://www.baidu.com") as f:
        data = f.read()
        print('Status:', f.status, f.reason)
        for k, v in f.getheaders():
            print('%s: %s' % (k, v))
        print('Data:', data.decode('utf-8'))


# 带请求头的GET方法
def test_get_with_headers():
    # 构造请求头
    req = request.Request("http://www.douban.com/")
    req.add_header('User-Agent',
                   'Mozilla/6.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/8.0 Mobile/10A5376e Safari/8536.25')
    with request.urlopen(req) as f:
        print('Status:', f.status, f.reason)
        for k, v in f.getheaders():
            print('%s: %s' % (k, v))
        print('Data:', f.read().decode('utf-8'))


if __name__ == '__main__':
    test_get()
    test_get_with_headers()
