"""
hashlib：摘要算法模块
"""

from hashlib import md5


def calc_md5(password: str) -> str:
    b = password.encode("utf-8")  # 1.将原始数据转为二进制数据
    md = md5()  # 2. 创建一个md5实例
    md.update(b)  # 3. 输入二进制数据给md实例
    return md.hexdigest()  # 4. 返回MD5的加密字符串


if __name__ == '__main__':
    print(calc_md5("123456"))
