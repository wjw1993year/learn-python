"""
上下文模块
"""

if __name__ == '__main__':
    f = None
    try:
        f = open("ContextlibTest.py", 'r', encoding="utf-8").__enter__()
        lines = f.readlines()

        print(type(lines))
        for line in lines:
            print(line, end="")
    finally:
        if f:
            f.close()
