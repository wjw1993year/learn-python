"""
使用python自带单元测试模块
"""
import unittest


class MyTestCase(unittest.TestCase):

    # 这是从unittest.TestCase继承过来的构造方法
    # def __init__(self, methodName: str = ...) -> None:
    #     super().__init__(methodName)

    def setUp(self) -> None:
        print("start test")

    def test_m1(self):
        print("m1")

    def test_m2(self):
        print("m2")

    def test_m3(self):
        print("m3")

    def tearDown(self) -> None:
        print("end test")


if __name__ == '__main__':
    # unittest.main()  # 测试所有用例
    # 自定义测试集
    suite = unittest.TestSuite()  # TestSuit：测试套
    suite.addTest(MyTestCase("test_m1"))
    suite.addTest(MyTestCase("test_m3"))
    #
    runner = unittest.TextTestRunner()
    runner.run(suite)
