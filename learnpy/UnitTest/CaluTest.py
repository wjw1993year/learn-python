"""
草稿纸
"""
from typing import List

import numpy as np


def test_matmul():
    W = [
        [1, 2],
        [-2, 1]
    ]
    W = np.asarray(W)
    x0 = [[0], [1]]
    x0 = np.asarray(x0)
    h = [[6, 4]]

    print(np.matmul(W, x0))

    print(np.matmul(h, W))


def test_Gain(join_prob: List, condi_prob: List):
    H_YcondiX = 0
    H_Y = 1
    for i in range(len(join_prob)):
        H_YcondiX -= join_prob[i] * np.log2(condi_prob[i])
    print(H_Y - H_YcondiX)


if __name__ == '__main__':
    # test_matmul()
    # x1
    test_Gain([1 / 8, 3 / 8, 2 / 8, 2 / 8], [1 / 3, 3 / 5, 2 / 3, 2 / 5])
    # x2
    test_Gain([1 / 8, 3 / 8, 3 / 8, 1 / 8], [1 / 4, 3 / 4, 3 / 4, 1 / 4])
    # x3
    test_Gain([1 / 8, 3 / 8, 1 / 8, 3 / 8], [1 / 2, 3 / 6, 1 / 2, 3 / 6])
