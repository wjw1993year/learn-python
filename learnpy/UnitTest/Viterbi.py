"""
HMM中的维特比算法
"""
from typing import List

from learnpy.UnitTest import ViterbiInput


class Solution:

    #
    def viterbi(self, A: List[List], B: List[List], series: str):
        T = len(series)
        idx_list = self.symbol2Idx(series)
        n_state = len(A)
        viterbi_mat = [[0 for col in range(T + 1)] for row in range(n_state)]
        path_mat = [[0 for col in range(T + 1)] for row in range(n_state)]
        viterbi_mat[0][0] = 1  # 初始化
        # 外循环，遍历观察序列
        for t in range(1, T + 1):
            obs = idx_list[t - 1]
            # 内循环，求最大值和最大点
            for j in range(n_state):
                max_jt = 0
                maxIdx_jt = 0
                for i in range(n_state):
                    # 计算viterbi_mat[t - 1][i] * a_ij * b(obs | s_i)
                    tmp = viterbi_mat[i][t - 1] * A[i][j] * B[j][obs]
                    if tmp > max_jt:
                        max_jt = tmp
                        maxIdx_jt = i
                viterbi_mat[j][t] = max_jt
                path_mat[j][t] = maxIdx_jt

        # 回溯，得到最优路径
        path = []
        # 先弄最后一个
        max_Tj = 0
        maxIdx_Tj = 0
        for j in range(n_state):
            if viterbi_mat[j][T] > max_Tj:
                max_Tj = viterbi_mat[j][T]
                maxIdx_Tj = j
        path.append(maxIdx_Tj)

        for t in range(0, T):
            t = T - t
            fr = path[len(path) - 1]
            path.append(path_mat[fr][t])

        path.reverse()
        path = self.idx2Symbol(path)

        return viterbi_mat, path, path_mat

    def symbol2Idx(self, series: str):
        sym2Idx = {"0": 0, "A": 1, "C": 2, "G": 3, "T": 4,
                   "a": 5, "c": 6, "g": 7, "t": 8}
        out = [sym2Idx[ch] for ch in series]
        return out

    def idx2Symbol(self, series):
        idx2Sym = {0: "0", 1: "A", 2: "C", 3: "G", 4: "T",
                   5: "a", 6: "c", 7: "g", 8: "t"}
        out = [idx2Sym[ch] for ch in series]
        return out


if __name__ == '__main__':
    A = ViterbiInput.A
    B = ViterbiInput.B

    series = "CGCG"
    solution = Solution()
    viterbi_mat, path, path_mat = solution.viterbi(A, B, series)
    import numpy as np

    v_mat = np.asarray(viterbi_mat)
    p_mat = np.asarray(path_mat, dtype=np.int)
    np.savetxt("v_mat.csv", v_mat, delimiter=',')
    np.savetxt("p_mat.csv", p_mat, delimiter=',')
    print(v_mat)
    print(p_mat)
