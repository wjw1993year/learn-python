import re


def readRegFile():
    file = "regex.txt"
    l = []
    with open(file, encoding="utf-8") as f:
        for line in f:
            l = line.split(",")
            break
    return l


if __name__ == '__main__':
    l = readRegFile()
    reg_str = "|".join(l)
    reg = re.compile(reg_str)
    print(re.match(reg, "("))
