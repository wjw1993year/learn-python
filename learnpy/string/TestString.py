"""
测试内置的str
"""


# 用空格合并多个字符串为一个句子
def test_join():
    l = ["i", "am", "carman"]
    print(" ".join(l))


if __name__ == '__main__':
    test_join()
