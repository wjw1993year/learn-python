"""
面向对象编程
"""


class Student(object):

    def __init__(self, name, score):
        self.name = name
        self.score = score
        # 双下划线开头的属性是私有属性
        self.__grade = None

    # 双下划线开头表示私有方法
    def __private_method(self):
        print("private_method")

    @classmethod
    def clazz_m1(cls):
        print("clazz_m1")


if __name__ == '__main__':
    s = Student("", "")
    print(type(Student))
