"""
链表工具类
"""
from typing import List


class Node(object):
    def __init__(self, val, next):
        self.val = val
        self.next = next


def getLinkedList(arr: List):
    length = len(arr)
    if length == 0:
        return None
    # 尾插法
    head = Node(arr[0], None)
    tail = head
    for i in range(1, length):
        nxt = Node(arr[i], None)
        tail.next = nxt
        tail = nxt
    return head


def printLinkedList(node):
    while node:
        print(node.val, end=", ")
        node = node.next
    print()


if __name__ == '__main__':
    my_list = [0, 1, 2, 3, 4]
    head = getLinkedList(my_list)
    printLinkedList(head)
