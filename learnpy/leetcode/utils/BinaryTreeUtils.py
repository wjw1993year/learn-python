"""
二叉树工具类
"""
import collections


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def buildBinaryTree(values, adjList: dict, TreeNode):
    length = len(values)
    if len(values) == 0:
        return None
    node_list = [TreeNode(values[i]) for i in range(length)]
    root = node_list[0]
    for key in adjList:
        leftIdx, rightIdx = adjList.get(key)
        node = node_list[key]
        if leftIdx != -1:
            node.left = node_list[leftIdx]
        if rightIdx != -1:
            node.right = node_list[rightIdx]
    return root


def bfsPrint(root):
    if root is None:
        print("null")
    que = collections.deque()
    que.append(root)
    print(root.val, end=", ")
    print()
    while que:
        size = len(que)
        for i in range(size):
            node = que.popleft()
            if node.left:
                que.append(node.left)
                print(node.left.val, end=", ")
            else:
                print("null", end=", ")
            if node.right:
                que.append(node.right)
                print(node.right.val, end=", ")
            else:
                print("null", end=", ")
        print()


# 调用示例
# // 先生成如下结构的二叉树，然后逐层打印出来
# //      0
# //     / \
# //    1   2
# //     \   \
# //      3   4
#
if __name__ == '__main__':
    values = [0, 1, 2, 3, 4]
    adjList = {0: (1, 2), 1: (-1, 3), 2: (-1, 4)}
    root = buildBinaryTree(values, adjList, TreeNode)
    bfsPrint(root)
