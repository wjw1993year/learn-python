"""
leetcode876题
"""
from learnpy.leetcode.utils.LinkedListUtils import getLinkedList


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    # 快慢指针，当链表长度为偶数时，有两个中点：
    # 允许fast指向null，slow指向后中点
    # 不允许fast指向null，slow指向前中点
    def middleNode(self, head: ListNode) -> ListNode:
        slow = head
        fast = head
        while fast and fast.next:
            fast = fast.next.next
            slow = slow.next
        return slow


if __name__ == '__main__':
    head = getLinkedList([1, 2, 3, 4, 5])
    solution = Solution()
    print(solution.middleNode(head).val)
