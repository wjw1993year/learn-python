"""
leetcode206
"""
from learnpy.leetcode.utils.LinkedListUtils import getLinkedList, \
    printLinkedList


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    # 双指针
    def reverseList(self, head: ListNode) -> ListNode:
        prev = None
        cur = head
        while cur:
            nxtCur = cur.next
            nxtPrev = cur
            cur.next = prev
            prev = nxtPrev
            cur = nxtCur
        return prev


if __name__ == '__main__':
    head = getLinkedList([1, 2, 3, 4, 5])
    solution = Solution()
    printLinkedList(solution.reverseList(head))
