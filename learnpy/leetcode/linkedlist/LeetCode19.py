"""
leetcode19题
"""
from learnpy.leetcode.utils.LinkedListUtils import getLinkedList, \
    printLinkedList


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    # 快慢指针
    # 让fast先走n步，然后fast、slow一起走，直到fast指向null位置
    def removeNthFromEnd(self, head: ListNode, n: int) -> ListNode:
        fast = head
        slow = head
        prev_slow = None
        for i in range(n):
            fast = fast.next
        while fast:
            fast = fast.next
            prev_slow = slow
            slow = slow.next
        if prev_slow:
            prev_slow.next = slow.next
        else:
            head = head.next
        del slow
        return head


if __name__ == '__main__':
    nums = [1, 2, 3, 4, 5]
    n = 2
    head = getLinkedList(nums)
    solution = Solution()
    head = solution.removeNthFromEnd(head, n)
    printLinkedList(head)
