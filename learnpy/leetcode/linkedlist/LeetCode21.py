"""
leetcode21
"""
from typing import Optional

from learnpy.leetcode.utils.LinkedListUtils import getLinkedList, \
    printLinkedList


class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    # 用归并的方式合并
    # 尾插法
    def mergeTwoLists(self, list1: Optional[ListNode],
            list2: Optional[ListNode]) -> Optional[ListNode]:
        dummyHead = ListNode()
        tail = dummyHead
        p1, p2 = list1, list2
        while p1 and p2:
            if p1.val < p2.val:
                tail.next = p1
                tail = p1
                p1 = p1.next
            else:
                tail.next = p2
                tail = p2
                p2 = p2.next
        if p1 is not None:
            tail.next = p1
        else:
            tail.next = p2

        return dummyHead.next


if __name__ == '__main__':
    list1 = getLinkedList([1, 2, 4])
    list2 = getLinkedList([1, 3, 4])
    solution = Solution()
    merge = solution.mergeTwoLists(list1, list2)
    printLinkedList(merge)
