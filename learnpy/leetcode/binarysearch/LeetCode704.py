"""
leetcode704题
"""
from typing import List


class Solution:
    # 二分查找
    def search(self, nums: List[int], target: int) -> int:
        if len(nums) == 0:
            return -1
        left, right = 0, len(nums) - 1
        while left < right:
            m = left + (right - left) // 2  # //表示整数除法
            if target > nums[m]:
                left = m + 1
            else:
                right = m
        if nums[left] != target:
            return -1
        return left


if __name__ == '__main__':
    nums = [-1, 0, 3, 5, 9, 12]
    target = 9
    solution = Solution()
    print(solution.search(nums, target))
