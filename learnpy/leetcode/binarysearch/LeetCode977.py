"""
leetcode977题
"""
from typing import List


class Solution:

    # 双指针
    def sortedSquares(self, nums: List[int]) -> List[int]:

        ans = [0 for i in nums]
        left, right = 0, len(nums) - 1
        p = len(nums) - 1
        ## arr = list(map(lambda x: x ** 2, nums))
        arr = [x ** 2 for x in nums]
        while left <= right:
            if arr[left] > arr[right]:
                ans[p] = arr[left]
                left += 1
            else:
                ans[p] = arr[right]
                right -= 1
            p -= 1
        return ans


if __name__ == '__main__':
    nums = [-1, 1, 3, 5, 9, 12]
    solution = Solution()
    print(solution.sortedSquares(nums))
