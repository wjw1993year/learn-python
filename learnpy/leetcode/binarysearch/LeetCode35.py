"""
leetcode35题
"""
from typing import List


class Solution:
    def searchInsert(self, nums: List[int], target: int) -> int:
        if len(nums) == 0:
            return 0
        left, right = 0, len(nums)
        while left < right:
            m = left + (right - left) // 2
            if target > nums[m]:
                left = m + 1
            else:
                right = m

        return left


if __name__ == '__main__':
    nums = [1, 3, 5, 6]
    target = 5
    solution = Solution()
    print(solution.searchInsert(nums, target))
