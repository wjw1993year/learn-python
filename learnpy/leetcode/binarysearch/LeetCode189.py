"""
leetcode189题
"""
from typing import List


class Solution:
    # 翻转两次
    # 第一次翻转：整个数组翻转
    # 第二次翻转：[0,k)翻转，[k,len)翻转
    def rotate(self, nums: List[int], k: int) -> None:
        k = k % len(nums)
        self._reverse(nums, 0, len(nums) - 1)
        self._reverse(nums, 0, k - 1)
        self._reverse(nums, k, len(nums) - 1)

    def _reverse(self, nums, left, right):
        while left < right:
            tmp = nums[left]
            nums[left] = nums[right]
            nums[right] = tmp
            left += 1
            right -= 1


if __name__ == '__main__':
    nums = [1, 2, 3, 4, 5, 6, 7]
    k = 3
    solution = Solution()
    solution.rotate(nums, k)
    print(nums)
