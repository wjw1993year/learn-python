"""
leetcode322题
"""
from typing import List


class Solution:
    # dp：dp[i][j]表示coins[0,i)凑成j元的硬币个数
    # 初始化，dp[0][j：0~amount]为不可能的值，amount+1
    # 将两种情况的个数都计算出来：1、不使用coin[i-1]；2、使用n个coin[i-1]表示j
    def coinChange(self, coins: List[int], amount: int) -> int:
        MAX = amount + 1
        dp = [MAX for i in range(MAX)]
        dp[0] = 0  # 注意dp[0][0]=0

        # 这种写法效率较低，会超时
        # for coin in coins:
        #     for j in range(MAX):
        #         k = j // coin
        #         for n in range(1, k + 1):
        #             dp[j] = min(dp[j], n + dp[j - n * coin])

        for j in range(MAX):
            for coin in coins:
                if j >= coin:
                    dp[j] = min(dp[j], 1 + dp[j - coin])

        if dp[amount] == MAX:
            return -1
        return dp[amount]


if __name__ == '__main__':
    coins = [265, 398, 46, 78, 52]
    amount = 7754
    solution = Solution()
    print(solution.coinChange(coins, amount))
