"""
leetcode213题
"""
from typing import List


class Solution:
    def rob(self, nums: List[int]) -> int:
        length = len(nums)
        if length == 1:
            return nums[0]
        dp = [0 for i in range(length + 1)]
        # 第一轮，不取最后一个数
        dp[0] = 0
        dp[1] = nums[0]
        for k in range(2, length):
            dp[k] = max(dp[k - 1], dp[k - 2] + nums[k - 1])
        ans = dp[length - 1]
        # 第二轮，不取第一个数
        dp[1] = 0
        dp[2] = nums[1]
        for k in range(3, length + 1):
            dp[k] = max(dp[k - 1], dp[k - 2] + nums[k - 1])
        ans = max(ans, dp[length])
        return ans


if __name__ == '__main__':
    nums = [0]
    solution = Solution()

    print(solution.rob(nums))
