"""
leetcode5题
"""


class Solution:
    # 中心扩展
    def longestPalindrome(self, s: str) -> str:
        ans = ""
        ansL, ansR = 0, 0
        # 由单个字符进行扩展
        length = len(s)
        for i in range(length):
            left, right = self._span(i, i, s)
            if right - left - 1 > ansR - ansL:
                ansL, ansR = left + 1, right

        # 由双字符进行扩展
        for i in range(length - 1):
            left, right = self._span(i, i + 1, s)
            if right - left - 1 > ansR - ansL:
                ansL, ansR = left + 1, right
        return s[ansL:ansR]

    def _span(self, left: int, right: int, s: str) -> tuple:
        length = len(s)
        while left > -1 and right < length:
            if s[left] != s[right]:
                break
            left -= 1
            right += 1
        # 返回回文字符串的开区间范围(left, right)
        return left, right


if __name__ == '__main__':
    s = "aa"
    solution = Solution()
    print(solution.longestPalindrome(s))
