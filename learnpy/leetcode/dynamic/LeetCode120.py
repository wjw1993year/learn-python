"""
leetcode120题
"""
from typing import List


class Solution:
    # dp
    def minimumTotal(self, triangle: List[List[int]]) -> int:
        depth = len(triangle)
        dp = [[] for i in range(depth)]
        dp[0].append(triangle[0][0])
        INTEGER_MAX = 2147783647
        for i in range(1, depth):
            for j in range(i + 1):
                val = INTEGER_MAX
                if j > 0:
                    val = min(val, dp[i - 1][j - 1])
                if j < i:
                    val = min(val, dp[i - 1][j])
                val += triangle[i][j]
                dp[i].append(val)
        return min(dp[-1])


if __name__ == '__main__':
    triangle = [[2], [3, 4], [6, 5, 7], [4, 1, 8, 3]]
    solution = Solution()
    print(solution.minimumTotal(triangle))
