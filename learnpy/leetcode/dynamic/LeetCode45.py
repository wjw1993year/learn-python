"""
leetcode45题
"""
from typing import List


class Solution:
    # 贪心
    def jump(self, nums: List[int]) -> int:
        ans = 0
        maxIdx = 0
        length = len(nums)
        for i in range(length):
            if maxIdx >= length - 1:
                return ans
            # 搜索[i,maxIdx]范围的点，更新maxIdx
            oldMaxIdx = maxIdx
            for j in range(i, maxIdx + 1):
                maxIdx = max(maxIdx, nums[j] + j)
            if maxIdx > oldMaxIdx:
                ans += 1

        return ans


if __name__ == '__main__':
    nums = [7, 0, 9, 6, 9, 6, 1, 7, 9, 0, 1, 2, 9, 0, 3]
    soltion = Solution()
    print(soltion.jump(nums))
