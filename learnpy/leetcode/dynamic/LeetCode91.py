"""
leetcode91题
"""


class Solution:
    def numDecodings(self, s: str) -> int:
        length = len(s)
        dp = [0 for i in range(length + 1)]
        dp[0] = 1  # 空串看作可以解码
        if '1' <= s[0] <= '9':
            dp[1] = 1

        for k in range(2, length + 1):
            if '1' <= s[k - 1] <= '9':
                dp[k] += dp[k - 1]
            if '1' <= s[k - 2] <= '2' and 10 <= int(s[k - 2:k]) <= 26:
                dp[k] += dp[k - 2]
        return dp[length]


if __name__ == '__main__':
    s = "2261"
    solution = Solution()
    print(solution.numDecodings(s))
