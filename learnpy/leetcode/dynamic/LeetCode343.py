"""
leetcode343题
"""


class Solution:
    def integerBreak(self, n: int) -> int:
        # 特判
        if n <= 3:
            return n - 1

        res = n % 3
        if res == 0:
            return 3 ** (n // 3)
        elif res == 1:
            return 3 ** (n // 3 - 1) * 4
        else:
            return 3 ** (n // 3) * 2


if __name__ == '__main__':
    n = 15
    solution = Solution()
    print(solution.integerBreak(n))
