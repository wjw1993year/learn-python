"""
leetcode55题
"""
from typing import List


class Solution:

    # 动态规划
    def canJump(self, nums: List[int]) -> bool:
        length = len(nums)
        arr = [nums[length - i - 1] for i in range(length)]
        dp = [False for i in range(length)]
        dp[0] = True
        nearestTrueIdx = 0
        for curIdx in range(1, length):
            if arr[curIdx] >= curIdx - nearestTrueIdx:
                dp[curIdx] = True
                nearestTrueIdx = curIdx

        return dp[length - 1]


if __name__ == '__main__':
    nums = [3, 2, 1, 0, 4]
    solution = Solution()
    print(solution.canJump(nums))
