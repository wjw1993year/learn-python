"""
leetcode198题
"""
from typing import List


class Solution:
    def rob(self, nums: List[int]) -> int:
        length = len(nums)
        if length < 2:
            return nums[0]
        dp = [0 for i in range(length)]
        dp[0] = nums[0]
        dp[1] = max(nums[0], nums[1])
        for k in range(2, length):
            dp[k] = max(dp[k - 1], dp[k - 2] + nums[k])
        return dp[length - 1]


if __name__ == '__main__':
    nums = [2, 7, 9, 3, 1]
    solution = Solution()
    print(solution.rob(nums))
