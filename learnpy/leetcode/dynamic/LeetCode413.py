"""
leetcode413题
"""
from typing import List


class Solution:
    def numberOfArithmeticSlices(self, nums: List[int]) -> int:
        ans = 0
        length = len(nums)
        dp = [0 for i in range(length)]

        for k in range(2, length):
            if nums[k] - nums[k - 1] == nums[k - 1] - nums[k - 2]:
                dp[k] = dp[k - 1] + 1
            ans += dp[k]

        return ans


if __name__ == '__main__':
    nums = [1, 2, 3, 4]
    solution = Solution()
    print(solution.numberOfArithmeticSlices(nums))
