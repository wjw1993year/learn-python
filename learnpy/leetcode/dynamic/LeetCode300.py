"""
leetcode300题
"""
from typing import List


class Solution:
    # dp，o(n^2)
    def lengthOfLIS(self, nums: List[int]) -> int:
        length = len(nums)
        dp = [1 for i in range(length)]
        ans = 1
        for k in range(1, length):
            for j in range(k):
                if nums[k] > nums[j]:
                    dp[k] = max(dp[k], 1 + dp[j])
            ans = max(ans, dp[k])
        return ans


if __name__ == '__main__':
    nums = [10, 9, 2, 5, 3, 7, 101, 18]
    solution = Solution()
    print(solution.lengthOfLIS(nums))
