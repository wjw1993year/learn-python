# coding:utf-8
"""
leetcode166题：考察竖式除法
"""


class Solution:
    def fractionToDecimal(self, numerator: int, denominator: int) -> str:
        # 整数解，特判
        if numerator == 0:
            return "0"
        elif numerator % denominator == 0:
            return str(numerator // denominator)

        sign = "-" if (numerator > 0) ^ (denominator > 0) else ""
        a, b = abs(numerator), abs(denominator)
        # 整数部分
        integer = str(a // b)
        # 小数部分
        decimal = ""
        r = a % b
        r2pos_map = {}
        pos = 0
        while r != 0:
            if r in r2pos_map:
                pos = r2pos_map[r]
                decimal = decimal[0:pos] + "(" + decimal[pos:] + ")"
                break
            r2pos_map[r] = pos
            pos += 1
            r *= 10
            decimal += str(r // b)
            r = r % b
        ans = sign + integer + "." + decimal
        return ans


if __name__ == '__main__':
    solution = Solution()
    numerator = 22
    denominator = 7
    print(solution.fractionToDecimal(numerator, denominator))
