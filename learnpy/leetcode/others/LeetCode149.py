"""
leetcode149题
"""
from typing import List


class Solution:

    # 优化line2point的空间，改为line2cnt
    # 由于，num_of_line = num_of_point * (num_of_point-1) / 2
    # 所以, num_of_point - 1 < sqrt(2*num_of_line) < num_of_point
    # 故，num_of_point = int(sqrt(2*num_of_line)) + 1
    def maxPoints(self, points: List[List[int]]) -> int:
        n = len(points)
        # 特判
        if n < 2:
            return n

        line2cnt = {}
        for i in range(n - 1):
            for j in range(i + 1, n):
                line = self._line_of(points[i], points[j])
                line2cnt[line] = line2cnt.get(line, 0) + 1

        max_num_line = 0
        for v in line2cnt.values():
            max_num_line = max(max_num_line, v)

        ans = int((max_num_line * 2) ** 0.5) + 1
        return ans

    # line2point
    def maxPoints2(self, points: List[List[int]]) -> int:
        n = len(points)
        # 特判
        if n < 2:
            return n

        line2point = {}
        for i in range(n - 1):
            for j in range(i + 1, n):
                line = self._line_of(points[i], points[j])
                if line2point.get(line):
                    line2point.get(line).add(tuple(points[i]))
                    line2point.get(line).add(tuple(points[j]))
                else:
                    line2point[line] = {tuple(points[i]), tuple(points[j])}

        ans = 0
        for v in line2point.values():
            ans = max(ans, len(v))
        return ans

    def _line_of(self, p1: List, p2: List) -> tuple:
        x1, y1 = p1[0], p1[1]
        x2, y2 = p2[0], p2[1]
        dx, dy = x2 - x1, y2 - y1
        A, B, C = dy, -dx, dx * y1 - dy * x1
        gcd_ABC = self._gcd_of_3_nums(A, B, C)
        # 为确保单一性，保证A非负
        gcd_ABC = gcd_ABC if A >= 0 else -gcd_ABC
        return A // gcd_ABC, B // gcd_ABC, C // gcd_ABC

    def _gcd_of_3_nums(self, a: int, b: int, c: int) -> int:
        a, b, c = abs(a), abs(b), abs(c)
        gcd_ab = self._gcd(a, b)
        gcd_abc = self._gcd(gcd_ab, c)

        return gcd_abc

    def _gcd(self, a: int, b: int) -> int:
        if b == 0:
            return a
        return self._gcd(b, a % b)


if __name__ == '__main__':
    points = [[1, 1], [3, 2], [5, 3], [4, 1], [2, 3], [1, 4]]
    solution = Solution()
    print(solution.maxPoints(points))
