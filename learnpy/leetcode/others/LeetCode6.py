"""
leetcode6题
"""


class Solution:

    # 找规律：每个字符在结果中的索引从0增大到n-1，再从n-1降到0，如此反复
    # 例子如下：
    # [P, A, Y, P, A, L, I, S, H, I, R, I, N, G], numRows = 3
    # [0, 1, 2, 1, 0, 1, 2, 1, 0, 1, 2, 1, 0, 1]
    # 将索引0,1,2对应的字符汇总起来就得到结果了
    # 0：PAHN
    # 1：APLSIIG
    # 2：YIR
    def convert(self, s: str, numRows: int) -> str:
        # 特判，若numRows==1，则直接返回s
        if numRows < 2:
            return s
        rows = ["" for _ in range(numRows)]
        idx, flag = 0, 1

        for ch in s:
            rows[idx] += ch
            if idx == 0:
                flag = 1
            elif idx == numRows - 1:
                flag = -1
            idx += flag
        ans = "".join(rows)
        return ans


if __name__ == '__main__':
    solution = Solution()
    s = "PAYPALISHIRING"
    numRows = 3
    print(solution.convert(s, numRows))
