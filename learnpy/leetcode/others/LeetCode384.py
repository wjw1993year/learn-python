"""
leetcode384题
"""
import random
from typing import List


class Solution:
    def __init__(self, nums: List[int]):
        self.nums = nums
        self.original = nums.copy()

    def reset(self) -> List[int]:
        self.nums = self.original.copy()
        return self.nums

    def shuffle(self) -> List[int]:
        length = len(self.nums)
        for i in range(len(self.nums)):
            j = random.randrange(i, length)
            self.nums[i], self.nums[j] = self.nums[j], self.nums[i]
        return self.nums


if __name__ == '__main__':
    nums = [1, 2, 3]
    solution = Solution(nums)

    print(solution.shuffle())
    print(solution.reset())
