"""
leetcode39题
"""
from typing import List


class Solution:
    def combinationSum(self, candidates: List[int], target: int) -> \
            List[List[int]]:
        path = []
        ans = []
        arr = sorted(candidates)
        self._dfs(0, arr, 0, target, path, ans)
        return ans

    def _dfs(self, root: int, arr: List, curSum: int,
            target: int, path: List, ans: List):
        if curSum >= target:
            if curSum == target:
                ans.append(path.copy())
            return
        # 搜索子节点
        for i in range(root, len(arr)):
            # 剪枝
            if curSum + arr[i] > target:
                break
            curSum += arr[i]
            path.append(arr[i])
            self._dfs(i, arr, curSum, target, path, ans)
            # 回溯
            path.pop()
            curSum -= arr[i]


if __name__ == '__main__':
    candidates = [2, 3, 6, 7]
    target = 7
    solution = Solution()
    print(solution.combinationSum(candidates, target))
