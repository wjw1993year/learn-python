"""
leetcode40题
"""
from typing import List


class Solution:
    def combinationSum2(self, candidates: List[int], target: int) \
            -> List[List[int]]:
        ans = []
        path = []
        arr = sorted(candidates)
        self._dfs(0, arr, 0, target, path, ans)

        return ans

    # 去重措施，同一级子节点的大小要按照严格递增
    def _dfs(self, root: int, arr: List, curSum: int,
            target: int, path: List, ans: List):
        if curSum >= target:
            if curSum == target:
                ans.append(path.copy())
            return
        # 搜索子节点
        for i in range(root, len(arr)):
            # 剪枝1:同一级子节点的大小要按照严格递增
            if i - 1 >= root and arr[i] == arr[i - 1]:
                continue
            # 剪枝2:早停
            if curSum + arr[i] > target:
                break
            curSum += arr[i]
            path.append(arr[i])
            self._dfs(i + 1, arr, curSum, target, path, ans)
            path.pop()
            curSum -= arr[i]


if __name__ == '__main__':
    candidates = [10, 1, 2, 7, 6, 1, 5]
    target = 8
    solution = Solution()
    print(solution.combinationSum2(candidates, target))
