"""
leetcode200题
"""
from typing import List


class Solution:
    # 遍历图，统计连通分量的个数
    # 构图
    def numIslands(self, grid: List[List[str]]) -> int:
        ans = 0
        m, n = len(grid), len(grid[0])
        visit = [[0 for j in range(n)] for i in range(m)]
        for i in range(m):
            for j in range(n):
                if visit[i][j] == 0 and grid[i][j] == '1':
                    ans += 1
                    self._dfs(i, j, grid, visit)
        return ans

    def _dfs(self, i: int, j: int, grid: List[List[str]],
            visit: List[List[int]]):
        if visit[i][j] == 1:
            return
        # 搜索子节点
        m, n = len(visit), len(visit[0])
        visit[i][j] = 1
        # 上下左右四个子
        if i > 0 and grid[i - 1][j] == '1':
            self._dfs(i - 1, j, grid, visit)
        if i < m - 1 and grid[i + 1][j] == '1':
            self._dfs(i + 1, j, grid, visit)
        if j > 0 and grid[i][j - 1] == '1':
            self._dfs(i, j - 1, grid, visit)
        if j < n - 1 and grid[i][j + 1] == '1':
            self._dfs(i, j + 1, grid, visit)


if __name__ == '__main__':
    grid = [
        ["1", "1", "0", "0", "0"],
        ["1", "1", "0", "0", "0"],
        ["0", "0", "1", "0", "0"],
        ["0", "0", "0", "1", "1"]
    ]
    solution = Solution()
    print(solution.numIslands(grid))
