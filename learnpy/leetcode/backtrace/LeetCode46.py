"""
leetcode46题
"""
from typing import List


class Solution:
    # dfs
    def permute(self, nums: List[int]) -> List[List[int]]:
        ans = []
        path = []
        self._dfs(0, nums, path, ans)
        return ans

    def _dfs(self, root: int, arr: List, path: List, ans: List):
        if root == len(arr):
            ans.append(path.copy())
            return
        # 搜索子节点
        for i in range(root, len(arr)):
            path.append(arr[i])
            self._swap(arr, root, i)
            self._dfs(root + 1, arr, path, ans)
            # 回溯
            self._swap(arr, root, i)
            path.pop(len(path) - 1)

    def _swap(self, arr: List, i: int, j: int):
        tmp = arr[i]
        arr[i] = arr[j]
        arr[j] = tmp


if __name__ == '__main__':
    nums = [1, 2, 3]
    solution = Solution()
    print(solution.permute(nums))
