"""
leetcode797题
"""
from typing import List


class Solution:
    def allPathsSourceTarget(self, graph: List[List[int]]) -> List[List[int]]:
        ans = []
        path = []
        target = len(graph) - 1
        path.append(0)
        self._dfs(0, target, graph, path, ans)
        path.pop()  # 这行可以不写，这里为了保持回溯的代码形式才写的
        return ans

    # 回溯，注意有向无环图的回溯，不必用visit来记录结点是否被访问过
    def _dfs(self, root: int, target: int, adjList: List[List[int]], path: List,
            ans: List):
        if root == target:
            ans.append(path.copy())
            return

        # 搜索子结点
        for i in adjList[root]:
            path.append(i)
            self._dfs(i, target, adjList, path, ans)
            # 回溯
            path.pop()


if __name__ == '__main__':
    graph = [[4, 3, 1], [3, 2, 4], [3], [4], []]
    solution = Solution()
    print(solution.allPathsSourceTarget(graph))
