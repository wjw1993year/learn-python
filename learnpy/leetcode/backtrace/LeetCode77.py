"""
leetcode77题
"""
from typing import List


class Solution:

    # 回溯
    # 构图
    def combine(self, n: int, k: int) -> List[List[int]]:
        ans = []
        path = []
        arr = [i for i in range(1, n + 1)]
        self._dfs(0, arr, 0, k, path, ans)
        return ans

    def _dfs(self, root: int, arr: List, curLen: int, k: int, path: List,
            ans: List):
        if curLen == k:
            ans.append(path.copy())
            return
        # 搜索子节点
        x = len(arr) - k + curLen + 1
        for i in range(root, x):
            path.append(arr[i])
            self._dfs(i + 1, arr, curLen + 1, k, path, ans)
            # 回溯
            path.pop(len(path) - 1)


if __name__ == '__main__':
    n = 4
    k = 2
    solution = Solution()
    print(solution.combine(n, k))
