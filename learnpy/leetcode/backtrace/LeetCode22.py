"""
leetcode22题
"""
from typing import List


class Solution:
    def generateParenthesis(self, n: int) -> List[str]:
        ans = []
        path = []
        cnt = {'(': n, ')': n}
        self._dfs(cnt, 0, 2 * n, path, ans)
        return ans

    def _dfs(self, cnt: dict, pathSum: int, length: int, path: List, ans: List):
        if len(path) == length:
            if pathSum == 0:
                out = "".join(path)
                ans.append(out)
            return

        # 搜索子节点
        for k, v in cnt.items():
            # 剪枝，保证符号够用、路径和始终大于等于0
            if v == 0 or (k == ')' and pathSum == 0):
                continue
            score = 1 if k == '(' else -1
            pathSum += score
            path.append(k)
            cnt[k] = cnt[k] - 1
            self._dfs(cnt, pathSum, length, path, ans)
            # 回溯
            pathSum -= score
            path.pop()
            cnt[k] = cnt[k] + 1


if __name__ == '__main__':
    n = 3
    solution = Solution()
    print(solution.generateParenthesis(n))
