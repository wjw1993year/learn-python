"""
Leetcode695:岛屿最大面积

"""
from typing import List


class Solution:
    def maxAreaOfIsland(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        visit = [[0 for j in range(n)] for i in range(m)]
        ans = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    area = self._dfs(i, j, grid, visit)
                    ans = max(ans, area)
        return ans

    def _dfs(self, i: int, j: int, grid: List[List[int]],
            visit: List[List[int]]) -> int:
        if visit[i][j] == 1:
            return 0
        # 搜索上下左右四个子节点
        m, n = len(grid), len(grid[0])
        area = 1
        visit[i][j] = 1  # 标记为已搜索状态
        if i > 0 and grid[i - 1][j] == 1:
            area += self._dfs(i - 1, j, grid, visit)
        if i < m - 1 and grid[i + 1][j] == 1:
            area += self._dfs(i + 1, j, grid, visit)
        if j > 0 and grid[i][j - 1] == 1:
            area += self._dfs(i, j - 1, grid, visit)
        if j < n - 1 and grid[i][j + 1] == 1:
            area += self._dfs(i, j + 1, grid, visit)

        return area


if __name__ == '__main__':
    grid = [[0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
            [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0],
            [0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0]]
    solution = Solution()
    ans = solution.maxAreaOfIsland(grid)
    print(ans)
