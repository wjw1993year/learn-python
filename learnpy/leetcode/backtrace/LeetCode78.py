"""
leetcod78题
"""
from typing import List


class Solution:
    # 构图
    def subsets(self, nums: List[int]) -> List[List[int]]:
        ans = []
        path: List[bool] = []
        self._dfs(path, nums, ans)
        return ans

    def _dfs(self, path: List[bool], arr: List, ans: List):
        if len(path) == len(arr):
            out = []
            for i, boolean in enumerate(path):
                if boolean:
                    out.append(arr[i])
            ans.append(out)
            return
        # 搜索子节点,两个子节点，分别为T,F
        for boolean in (False, True):
            path.append(boolean)
            self._dfs(path, arr, ans)
            path.pop()  # 回溯


if __name__ == '__main__':
    nums = [1, 2, 3]
    solution = Solution()
    print(solution.subsets(nums))
