"""
leetcode542题
"""
import collections
from typing import List


class Solution:
    # 多源广度优先搜索
    # 0只能作为根节点，1只能作为子孙节点
    def updateMatrix(self, mat: List[List[int]]) -> List[List[int]]:
        m, n = len(mat), len(mat[0])
        visit = [[0 for col in range(n)] for row in range(m)]
        ans = [[0 for col in range(n)] for row in range(m)]
        que = collections.deque()
        for i in range(m):
            for j in range(n):
                if mat[i][j] == 0:
                    visit[i][j] = 1
                    que.append((i, j))
        # 逐层搜索
        layer = 1
        while que:
            size = len(que)
            for i in range(size):
                # 搜索上下左右四个子节点
                x, y = que.popleft()
                if x > 0 and visit[x - 1][y] == 0:
                    ans[x - 1][y] = layer
                    visit[x - 1][y] = 1
                    que.append((x - 1, y))
                if x < m - 1 and visit[x + 1][y] == 0:
                    ans[x + 1][y] = layer
                    visit[x + 1][y] = 1
                    que.append((x + 1, y))
                if y > 0 and visit[x][y - 1] == 0:
                    ans[x][y - 1] = layer
                    visit[x][y - 1] = 1
                    que.append((x, y - 1))
                if y < n - 1 and visit[x][y + 1] == 0:
                    ans[x][y + 1] = layer
                    visit[x][y + 1] = 1
                    que.append((x, y + 1))
            layer += 1
        return ans


if __name__ == '__main__':
    mat = [[0, 0, 0], [0, 1, 0], [1, 1, 1]]
    solution = Solution()
    ans = solution.updateMatrix(mat)
    print(ans)
