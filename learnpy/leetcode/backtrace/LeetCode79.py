"""
leetcode79题
"""
from typing import List


class Solution:
    def __init__(self):
        self.isExist = False
        self.offset = [(-1, 0), (1, 0), (0, -1), (0, 1)]

    def exist(self, board: List[List[str]], word: str) -> bool:
        m, n = len(board), len(board[0])
        visit = [[False for j in range(n)] for i in range(m)]
        for i in range(m):
            for j in range(n):
                if self.isExist:
                    return True
                if board[i][j] == word[0]:
                    visit[i][j] = True
                    self._dfs(i, j, board, word, 1, visit)
                    visit[i][j] = False

        return self.isExist

    # 在图中，搜索出特定的路径
    def _dfs(self, i: int, j: int, board: List, word: str,
            hasMatchLen: int, visit: List[List[bool]]):
        if self.isExist:
            return
        if hasMatchLen == len(word):
            self.isExist = True
            return
        # 继续搜索上下左右子节点
        nxtCh = word[hasMatchLen]
        m, n = len(board), len(board[0])
        for dx, dy in self.offset:
            x, y = i + dx, j + dy
            # 检查合法性
            if x > m - 1 or x < 0 or y > n - 1 or y < 0:
                continue
            if not visit[x][y] and board[x][y] == nxtCh:
                hasMatchLen += 1
                visit[x][y] = True
                self._dfs(x, y, board, word, hasMatchLen, visit)
                # 回溯
                visit[x][y] = False
                hasMatchLen -= 1


if __name__ == '__main__':
    board = [
        ["A", "B", "C", "E"],
        ["S", "F", "C", "S"],
        ["A", "D", "E", "E"]
    ]
    word = "ABCCED"
    solution = Solution()
    print(solution.exist(board, word))
