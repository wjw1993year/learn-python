"""
leetcode1091题
"""
import collections
from typing import List


class Solution:
    # bfs寻找最小路径长度
    def shortestPathBinaryMatrix(self, grid: List[List[int]]) -> int:
        # 特判
        if grid[0][0] == 1:
            return -1
        n = len(grid)
        visit = [[False for j in range(n)] for i in range(n)]
        que = collections.deque()
        que.append((0, 0))
        visit[0][0] = True
        layer = 1
        while que:
            # 逐层搜索目标点(n-1, n-1)
            size = len(que)
            for k in range(size):
                (x, y) = que.popleft()
                # 判断当前根结点是否为目标点
                if (x, y) == (n - 1, n - 1):
                    return layer
                # 搜索周围的八个子结点
                for i in range(x - 1, x + 2):
                    # 判断子节点的存在性
                    if i < 0 or i >= n:
                        continue
                    for j in range(y - 1, y + 2):
                        # 子节点的存在性（索引在grid范围内且数值为0）、
                        # 是否被访问过
                        if j < 0 or j >= n or grid[i][j] != 0 or visit[i][j]:
                            continue
                        que.append((i, j))
                        visit[i][j] = True
            layer += 1
        # 到最后也没有搜索到目标点，返回-1
        return -1


if __name__ == '__main__':
    grid = [
        [0, 0, 0],
        [1, 1, 0],
        [1, 1, 0]
    ]
    solution = Solution()
    print(solution.shortestPathBinaryMatrix(grid))
