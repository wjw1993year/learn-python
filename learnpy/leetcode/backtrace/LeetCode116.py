"""
leetcode116题
"""
import collections
from typing import Optional

from learnpy.leetcode.utils.BinaryTreeUtils import buildBinaryTree
from learnpy.leetcode.utils.LinkedListUtils import printLinkedList


class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None,
            next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next


class Solution:
    # bfs层序遍历
    def connect(self, root: 'Optional[Node]') -> 'Optional[Node]':
        if root is None:
            return None
        node = root
        que = collections.deque()
        que.append(node)
        while que:
            size = len(que)
            for i in range(size - 1):
                p = que.popleft()
                p.next = que[0]
                if p.left:
                    que.append(p.left)
                if p.right:
                    que.append(p.right)
            # 处理最后一个，指向None
            p = que.popleft()
            if p.left:
                que.append(p.left)
            if p.right:
                que.append(p.right)
        return root


if __name__ == '__main__':
    values = [0, 1, 2, 3, 4, 5, 6]
    adjList = {0: (1, 2), 1: (3, 4), 2: (5, 6)}
    root = buildBinaryTree(values, adjList, Node)

    solution = Solution()
    solution.connect(root)
    printLinkedList(root)
    printLinkedList(root.left)
    printLinkedList(root.left.left)
    