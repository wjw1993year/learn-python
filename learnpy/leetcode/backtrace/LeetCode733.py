"""
leetcode733题
"""
import collections
from typing import List


class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int,
            newColor: int) -> List[List[int]]:
        oldColor = image[sr][sc]
        img = image.copy()
        # 特判：由于只能修改oldcolor，若oldclord与newcolor相同，则无需修改了
        if oldColor == newColor:
            return img
        self._bfs(sr, sc, img, oldColor, newColor)
        return img

    def _dfs(self, i: int, j: int, img: List[List[int]], oldColor, newColor):
        # 停止条件
        if img[i][j] != oldColor:
            return
        img[i][j] = newColor  # 标记为已搜索状态
        # 继续搜索子节点，上、下、左、右
        m, n = len(img), len(img[0])
        if i > 0:
            self._dfs(i - 1, j, img, oldColor, newColor)
        if i < m - 1:
            self._dfs(i + 1, j, img, oldColor, newColor)
        if j > 0:
            self._dfs(i, j - 1, img, oldColor, newColor)
        if j < n - 1:
            self._dfs(i, j + 1, img, oldColor, newColor)

    # 构建有向无环图
    def _bfs(self, sr: int, sc: int, img: List[List[int]], oldColor, newColor):
        que = collections.deque()
        img[sr][sc] = newColor
        que.append((sr, sc))
        m, n = len(img), len(img[0])
        # 搜索子节点，子节点在上下左右，并且颜色为oldColor
        while que:
            i, j = que.popleft()
            if i > 0 and img[i - 1][j] == oldColor:
                img[i - 1][j] = newColor  # 标记为已访问
                que.append((i - 1, j))
            if i < m - 1 and img[i + 1][j] == oldColor:
                img[i + 1][j] = newColor  # 标记为已访问
                que.append((i + 1, j))
            if j > 0 and img[i][j - 1] == oldColor:
                img[i][j - 1] = newColor
                que.append((i, j - 1))
            if j < n - 1 and img[i][j + 1] == oldColor:
                img[i][j + 1] = newColor
                que.append((i, j + 1))


if __name__ == '__main__':
    image = [[1, 1, 1], [1, 1, 0], [1, 0, 1]]
    sr, sc, newColor = 1, 1, 2
    solution = Solution()
    img = solution.floodFill(image, sr, sc, newColor)
    print(img)
