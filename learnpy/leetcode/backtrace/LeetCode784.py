"""
leetcode784题
"""
from typing import List


class Solution:
    # 思路是求幂集
    def letterCasePermutation(self, s: str) -> List[str]:
        ans = []
        arr = [ch.lower() for ch in s]
        pos2idx = []
        for idx in range(len(arr)):
            ch = arr[idx]
            if 'a' <= ch <= 'z':
                pos2idx.append(idx)
        n = len(pos2idx)
        # 用[0,2^n)表示[0,n)的幂集
        for i in range(1 << n):
            path = arr.copy()
            for j in range(n):
                if (i & (1 << j)) > 0:
                    idx = pos2idx[j]
                    path[idx] = path[idx].upper()
            ans.append("".join(path))
        return ans


if __name__ == '__main__':
    s = "a1b2"
    solution = Solution()
    print(solution.letterCasePermutation(s))
