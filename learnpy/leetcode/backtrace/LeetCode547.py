"""
leetcode547题
"""
from typing import List


class Solution:
    # 通过邻接矩阵统计连通分量个数
    def findCircleNum(self, isConnected: List[List[int]]) -> int:
        ans = 0
        n = len(isConnected)
        visit = [0 for j in range(n)]
        for i in range(n):
            if visit[i] == 0:
                self._dfs(i, isConnected, visit)
                ans += 1
        return ans

    def _dfs(self, root: int, isConnected: List[List[int]], visit: List[int]):
        if visit[root] == 1:
            return
        # 访问子节点
        visit[root] = 1
        n = len(isConnected)
        for j in range(n):
            if j == root or isConnected[root][j] == 0:
                continue
            self._dfs(j, isConnected, visit)


if __name__ == '__main__':
    isConnected = [
        [1, 1, 0],
        [1, 1, 0],
        [0, 0, 1]
    ]
    solution = Solution()
    print(solution.findCircleNum(isConnected))
