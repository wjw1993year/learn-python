"""
leetcode47题
"""
from typing import List


class Solution:
    def permuteUnique(self, nums: List[int]) -> List[List[int]]:
        cnt = {}
        for num in nums:
            cnt[num] = cnt.get(num, 0) + 1
        path = []
        ans = []
        self._dfs(cnt, len(nums), path, ans)
        return ans

    # 构图，root的子节点不重复即可
    def _dfs(self, cnt: dict, targetLen: int, path: List[int], ans: List):
        if targetLen == len(path):
            ans.append(path.copy())
            return
        # 搜索子节点
        for k, v in cnt.items():
            if v == 0:
                continue
            path.append(k)
            cnt[k] = cnt[k] - 1
            self._dfs(cnt, targetLen, path, ans)
            cnt[k] = cnt[k] + 1
            path.pop()


if __name__ == '__main__':
    nums = [1, 1, 2]
    solution = Solution()
    print(solution.permuteUnique(nums))
