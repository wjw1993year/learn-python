"""
leetcode994题
"""
import collections
from typing import List


class Solution:
    # 多源广度优先搜索
    def orangesRotting(self, grid: List[List[int]]) -> int:
        m, n = len(grid), len(grid[0])
        visit = [[0 for col in range(n)] for row in range(m)]
        que = collections.deque()
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 0:
                    visit[i][j] = 1
                elif grid[i][j] == 2:
                    visit[i][j] = 1
                    que.append((i, j))
        ans = 0
        while que:
            size = len(que)
            isTrans = False  # 记录这一层是否发生了腐烂的传染
            for i in range(size):
                x, y = que.popleft()
                # 搜索上下左右
                if x > 0 and visit[x - 1][y] == 0:
                    visit[x - 1][y] = 1
                    que.append((x - 1, y))
                    isTrans = True
                if x < m - 1 and visit[x + 1][y] == 0:
                    visit[x + 1][y] = 1
                    que.append((x + 1, y))
                    isTrans = True
                if y > 0 and visit[x][y - 1] == 0:
                    visit[x][y - 1] = 1
                    que.append((x, y - 1))
                    isTrans = True
                if y < n - 1 and visit[x][y + 1] == 0:
                    visit[x][y + 1] = 1
                    que.append((x, y + 1))
                    isTrans = True
            if isTrans:
                ans += 1
        # 检查是否还有新鲜的
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1 and visit[i][j] == 0:
                    return -1

        return ans


if __name__ == '__main__':
    grid = [[2, 1, 1], [1, 1, 0], [0, 1, 1]]
    solution = Solution()
    ans = solution.orangesRotting(grid)
    print(ans)
