"""
leetcode90题
"""
from typing import List


class Solution:

    # 构图，TF图，父与子相同时，不允许FT这种组合
    def subsetsWithDup(self, nums: List[int]) -> List[List[int]]:
        path = []
        ans = []
        arr = sorted(nums)
        self._dfs(arr, path, ans)
        return ans

    def _dfs(self, nums: List[int], path: List[bool], ans: List[List[int]]):
        root = len(path)
        if root == len(nums):
            out = []
            for idx, boolean in enumerate(path):
                if boolean:
                    out.append(nums[idx])
            ans.append(out)
            return

        # 搜索子节点
        for boolean in (True, False):
            # root-1指向当前根节点，root指向子节点
            if boolean and root > 0 and nums[root - 1] == nums[root] \
                    and not path[root - 1]:
                continue
            path.append(boolean)
            self._dfs(nums, path, ans)
            path.pop()


if __name__ == '__main__':
    nums = [1, 2, 2]
    solution = Solution()
    print(solution.subsetsWithDup(nums))
