"""
leetcode117题
"""
from learnpy.leetcode.utils.BinaryTreeUtils import buildBinaryTree
from learnpy.leetcode.utils.LinkedListUtils import printLinkedList


class Node:
    def __init__(self, val: int = 0, left: 'Node' = None, right: 'Node' = None,
            next: 'Node' = None):
        self.val = val
        self.left = left
        self.right = right
        self.next = next


class Solution:
    def connect(self, root: 'Node') -> 'Node':
        # 特判
        if root is None:
            return root
        dummyHead = Node()
        dummyHead.next = root
        # 外循环:层间
        while dummyHead.next:
            p = dummyHead.next
            nxtHead = Node()
            tail = nxtHead
            while p:
                tail = self._handle_connect(p, tail)
                p = p.next
            dummyHead = nxtHead
        return root

    def _handle_connect(self, p: 'Node', tail: 'Node') -> 'Node':
        if p.left:
            tail.next = p.left
            tail = tail.next
        if p.right:
            tail.next = p.right
            tail = tail.next
        return tail


if __name__ == '__main__':
    values = [1, 2, 3, 4, 5, 7]
    adjList = {0: (1, 2), 1: (3, 4), 2: (-1, 5)}
    root = buildBinaryTree(values, adjList, Node)
    solution = Solution()
    solution.connect(root)
    printLinkedList(root)
    printLinkedList(root.left)
    printLinkedList(root.left.left)
