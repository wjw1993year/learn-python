"""
leetcode572题
"""
from learnpy.leetcode.utils.BinaryTreeUtils import buildBinaryTree


class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    # subRoot是root的子树，满足其一即可：
    # 1.subRoot与root相等
    # 或者2.subRoot是root.left的子树，
    # 或者3.subRoot是root.right的子树
    def isSubtree(self, root: TreeNode, subRoot: TreeNode) -> bool:
        # 注意：空树是任何树的子树，但本题给定subRoot都不为None，所以subRoot is None可以不写
        if subRoot is None or self._isEquals(root, subRoot):
            return True
        # 到这里，说明subRoot不为None
        if root is None:
            return False

        # 到这里，说明
        return self.isSubtree(root.left, subRoot) or self.isSubtree(root.right,
                                                                    subRoot)

    def _isEquals(self, root1: TreeNode, root2: TreeNode) -> bool:
        if root1 is None and root2 is None:
            return True

        if root1 is None or root2 is None:
            return False

        return root1.val == root2.val \
               and self._isEquals(root1.left, root2.left) \
               and self._isEquals(root1.right, root2.right)


if __name__ == '__main__':
    values = [3, 4, 5, 1, 2]
    adjList = {0: (1, 2), 1: (3, 4)}
    root = buildBinaryTree(values, adjList, TreeNode)

    values = [4, 1, 2]
    adjList = {0: (1, 2)}
    subRoot = buildBinaryTree(values, adjList, TreeNode)

    solution = Solution()
    print(solution.isSubtree(root, subRoot))
