"""
leetcode231题
"""


class Solution:
    # 技巧1，若为2的幂，则n&(n-1)==0 且 n>0
    def isPowerOfTwo(self, n: int) -> bool:
        return n > 0 and (n & (n - 1)) == 0

    # 技巧2 lowbit运算，若为2的幂，则n&(-n)==n 且 n>0
    def isPowerOfTwo2self(self, n: int) -> bool:
        return n > 0 and (n & -n) == n


if __name__ == '__main__':
    pass
