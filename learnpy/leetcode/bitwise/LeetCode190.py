"""
leetcode190题
"""


class Solution:
    # 逐级的二进制为相加，不会有进位发生，可以用异或求和，加快效率
    def reverseBits(self, n: int) -> int:
        ans = 0

        for j in range(32):
            if n & (1 << j) != 0:
                ans ^= 1 << (31 - j)
        return ans


if __name__ == '__main__':
    n = 43261596
    solution = Solution()
    print(solution.reverseBits(n))
