"""
leetcode191题
"""


class Solution:
    # 位与运算
    def hammingWeight(self, n: int) -> int:
        ans = 0
        for j in range(32):
            if n & (1 << j) != 0:
                ans += 1
        return ans


if __name__ == '__main__':
    n = -2
    solution = Solution()
    print(solution.hammingWeight(n))
