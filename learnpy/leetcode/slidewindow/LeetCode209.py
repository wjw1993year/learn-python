"""
leetcode209题
"""
from typing import List


class Solution:
    # 滑窗
    def minSubArrayLen(self, target: int, nums: List[int]) -> int:
        left, right = 0, 0
        length = len(nums)
        ans = length + 1
        window = 0
        while right < length:
            num = nums[right]
            if self._isCanMoveRight(target, window):
                window += num
                right += 1
                continue

            while not self._isCanMoveRight(target, window):
                ans = min(ans, right - left)
                window -= nums[left]
                left += 1

        # 处理最后一个窗口[left, len）
        while not self._isCanMoveRight(target, window):
            ans = min(ans, right - left)
            window -= nums[left]
            left += 1

        if ans != length + 1:
            return ans

        return 0

    def _isCanMoveRight(self, threshold, window):
        return window < threshold


if __name__ == '__main__':
    target = 7
    nums = [2, 3, 1, 2, 4, 3]
    solution = Solution()
    print(solution.minSubArrayLen(target, nums))
