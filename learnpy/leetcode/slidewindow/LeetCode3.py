"""
leetcode3题
"""


class Solution:
    # 滑窗
    def lengthOfLongestSubstring(self, s: str) -> int:
        ans = 0
        left, right = 0, 0
        length = len(s)
        window = set()
        while right < length:
            ch = s[right]
            if self._isCanMoveRight(window, ch):
                window.add(ch)
                right += 1
                continue
            # 维护最优解
            ans = max(ans, len(window))
            while not self._isCanMoveRight(window, ch):
                window.remove(s[left])
                left += 1
        # 处理最后一个窗口[left, len)
        ans = max(ans, right - left)
        return ans

    def _isCanMoveRight(self, window: set, ch: str):
        return ch not in window


if __name__ == '__main__':
    s = "abcabcbb"
    solution = Solution()
    print(solution.lengthOfLongestSubstring(s))
