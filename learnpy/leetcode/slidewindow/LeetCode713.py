"""
leetcode713题
"""
from typing import List


class Solution:
    # 滑窗
    def numSubarrayProductLessThanK(self, nums: List[int], k: int) -> int:
        ans = 0
        left, right = 0, 0
        length = len(nums)
        window = 1
        while right < length:
            num = nums[right]
            if self._checkCanMoveRight(k, window, num):
                window *= num
                right += 1
                continue
            elif left == right:
                # 跳窗，不能右移，且nums[right]>k，只能跳过这个数
                left += 1
                right += 1
                window = 1
                continue

            # 维护答案
            while not self._checkCanMoveRight(k, window, num):
                if left == right:
                    # 跳窗
                    break
                ans += right - left  # nums[left,right)包含nums[left]的子数组都符合
                window //= nums[left]
                left += 1

        # 处理最后一个窗口[left,length)
        n = length - left
        ans += n * (n + 1) // 2  # 1+2+3+...+n
        return ans

    def _checkCanMoveRight(self, threshold, window, num):
        return window * num < threshold
    

if __name__ == '__main__':
    nums = [10, 5, 2, 6]
    k = 100
    solution = Solution()
    print(solution.numSubarrayProductLessThanK(nums, k))
