"""
leetcode567题
"""


class Solution:
    # 滑动窗口
    def checkInclusion(self, s1: str, s2: str) -> bool:
        # 特判
        if len(s1) > len(s2):
            return False

        s1_map = {}
        for ch in s1:
            s1_map[ch] = s1_map.get(ch, 0) + 1
        window = {}
        left, right = 0, 0
        hasMatch = 0
        while right < len(s2):
            ch = s2[right]
            if self._isCanMoveRight(s1_map, window, ch):
                window[ch] = window.get(ch, 0) + 1
                hasMatch += 1
                right += 1
                continue
            elif left == right:
                # 跳窗，不能移动right，并且left==right
                right += 1
                left = right
                window = {}
                hasMatch = 0
                continue
            if hasMatch == len(s1):
                return True
            while not self._isCanMoveRight(s1_map, window, ch):
                if left == right:
                    # 跳窗，不能移动right，并且left==right
                    break
                leftCh = s2[left]
                if window[leftCh] > 1:
                    window[leftCh] = window[leftCh] - 1
                else:
                    window.pop(leftCh)
                hasMatch -= 1
                left += 1
        # 处理最后一个窗口
        return hasMatch == len(s1)

    def _isCanMoveRight(self, s1_map: dict, window: dict, ch: str):
        return ch in s1_map and s1_map[ch] > window.get(ch, 0)


if __name__ == '__main__':
    s1 = "ab"
    s2 = "eidbaooo"
    solution = Solution()
    print(solution.checkInclusion(s1, s2))
