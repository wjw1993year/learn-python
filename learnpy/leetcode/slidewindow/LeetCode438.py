"""
leetcode438题
"""
from typing import List


class Solution:
    def findAnagrams(self, s: str, p: str) -> List[int]:
        ans = []
        # 特判
        if len(s) < len(p):
            return ans

        left, right = 0, 0
        p_map = {}
        window = {}
        hasMatch = 0
        for ch in p:
            p_map[ch] = p_map.get(ch, 0) + 1

        while right < len(s):
            ch = s[right]
            if self._isCanMoveRight(p_map, window, ch):
                window[ch] = window.get(ch, 0) + 1
                right += 1
                hasMatch += 1
                continue
            elif left == right:  # 不能移动right并且left==right
                # 跳窗，因为窗口可能不符合要求，所以要跳过去
                right += 1
                left = right
                hasMatch = 0
                window.clear()
                continue
            # 维护答案
            if hasMatch == len(p):
                ans.append(left)
            while not self._isCanMoveRight(p_map, window, ch):
                if left == right:
                    # 不能移动right并且left==right
                    break
                leftCh = s[left]
                if window[leftCh] > 1:
                    window[leftCh] = window[leftCh] - 1
                else:
                    window.pop(leftCh)
                hasMatch -= 1
                left += 1
        # 处理最后一个窗口[left, len)
        if hasMatch == len(p):
            ans.append(left)
        return ans

    def _isCanMoveRight(self, p_map: dict, window: dict, ch):
        return ch in p_map and p_map[ch] > window.get(ch, 0)


if __name__ == '__main__':
    s = "cbaebabacd"
    p = "abc"
    solution = Solution()
    print(solution.findAnagrams(s, p))
