from collections import deque

"""
learning demo
"""

d = dict()
d['a'] = 1
d.pop('a')

que = deque()
que.append(1)


def m1(name, age=0, height=0):
    return height


def m2():
    return "a", 1


if __name__ == '__main__':
    a = m2()
    k, v = m2()
    print(a)
    print(k, v)

    d = {'a': 1, 'b': 2}
    for key, value in d.items():
        print(type(key), value)

    for k, v in enumerate(d):
        print(k, v)

    print(m1("123"))
    print(m1("123", height=4, age=10))
