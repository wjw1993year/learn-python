"""
生成器练习
杨辉三角生成器
"""


def triangle_generator():
    pre = []
    while True:
        cur = [1]
        n = len(pre)
        pre.append(0)
        for idx in range(n):
            cur.append(pre[idx] + pre[idx + 1])
        yield cur
        pre = cur


if __name__ == '__main__':
    list_gen = (x for x in range(10))
    tg = triangle_generator()
    for i in range(10):
        print(next(list_gen), next(tg))
