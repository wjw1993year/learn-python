from functools import cmp_to_key
"""
sorted函数
"""


def cmp(o1: tuple, o2: tuple) -> int:
    if o1[0] < o2[0]:
        return -1
    elif o1[0] > o2[0]:
        return 1
    else:
        return o2[1] - o1[1]


if __name__ == '__main__':
    nums = [36, 5, -12, 9, -21]
    print(sorted(nums))
    print(sorted(nums, reverse=True))
    print(sorted(nums, key=abs))

    L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
    sL = sorted(L, key=cmp_to_key(cmp))
    print(sL)

