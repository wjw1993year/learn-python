"""
闭包
"""


def lazy_sum(*args):
    def summ():
        ax = 0
        for n in args:
            ax = ax + n
        return ax

    return summ


if __name__ == '__main__':
    f1 = lazy_sum(1, 2, 3)
    f2 = lazy_sum(4, 5, 6)
    print(f1())
    print(f2())
