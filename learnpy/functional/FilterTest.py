"""
filter()函数
"""


# 利用filter，输出[0,n)内的回文数
def is_palindrome(n):
    def is_pr(num: int):
        s = str(num)
        left, right = 0, len(s) - 1
        while left < right:
            if s[left] != s[right]:
                return False
            left += 1
            right -= 1
        return True

    return filter(is_pr, (x for x in range(n)))


if __name__ == '__main__':
    it = is_palindrome(1000)
    for pr in it:
        print(pr)
