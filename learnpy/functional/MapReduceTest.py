from functools import reduce
from typing import List
from collections.abc import Iterator

"""
map reduce练习
"""


# map()函数实现，首字母大写其余小写
def normalize(names: List[str]) -> Iterator:
    def norm_name(name: str) -> str:
        name = name.capitalize()
        return name
    return map(norm_name, names)


# map()函数实现，将字符串转为list
def str_to_arr(s: str) -> List[str]:
    return list(map(str, s))


# reduce()函数实现连乘
def prod(nums: List[int]) -> int:
    def mul(a: int, b: int):
        return a * b
    return reduce(mul, nums)


if __name__ == '__main__':
    it = normalize(['adam', 'LISA', 'barT'])
    print(str_to_arr("abd123"))
    print(it, list(it))
    print(prod([1, 2, 3, 4]))
