"""
装饰器：在不改变原函数代码、调用该原函数的地方的代码的前提下，增强原函数的功能
"""


# 装饰器
def decorator1(func):
    # 包裹器，*args, **kw代表原函数需要用到的参数
    def wrapper(*args, **kw):
        print("call %s" % func.__name__)  # 增强的代码
        func(*args, **kw)  # 原函数

    return wrapper


# @decorator1注解相当于在紧接在now函数定义后，运行语句 now1=log(now1)
@decorator1
def now1():
    print("2021-12-10")


# 如果decorator本身需要传入参数，那就需要编写一个返回decorator的高阶函数
def decorator_with_para(text: str):
    # 这里定义装饰器，装饰器里用到text参数
    def decorator2(func):
        def wrapper(*args, **kw):
            print(" %s: %s" % (text, func.__name__))
            func(*args, **kw)

        return wrapper

    return decorator2


# @decorator_with_para注解的运算分两步:
# 1.先算decorator_with_para("execute")，得到decorator2
# 2.再算@decorator2，即now2=decorator2(now2)
# 因此，总的效果就是：now2=decorator_with_para("execute")(now2)
@decorator_with_para("execute")
def now2():
    print("2021-12-10")


# @注解，不一定非要用在装饰器的场合，它的作用仅仅是执行了 `now=decorator(now)`这样的语句
# 用@注解强行改变now3函数，令其返回一个自定义的对象
def deco3(func):
    class myclazz:
        def __init__(self):
            self.f1 = func

        def f2(self):
            print("f2")

    return myclazz


@deco3
def now3():
    print("now3")


if __name__ == '__main__':
    now1()
    now2()
    obj = now3()
    obj.f1()
    obj.f2()
