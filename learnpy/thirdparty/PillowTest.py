"""
pillow包
"""
from PIL import Image

if __name__ == '__main__':
    # 打开一个jpg图像文件，注意是当前路径:
    with Image.open("image/cat.jpg") as im:
        w, h = im.size
        print('Original image size: %sx%s' % (w, h))
