import os


def print_file_name(dir):
    filelist = os.listdir(dir)
    for file_name in filelist:
        fullfile = os.path.join(dir, file_name)
        if not os.path.isdir(fullfile):
            print(file_name)


if __name__ == '__main__':
    dir = r"D:\MyWork\3. 治理团队的工作\2. 具体工作\CBSS权限穿透项目\相关文档\CBSS系统后台表\数据字典\数据字典"
    print_file_name(dir)
