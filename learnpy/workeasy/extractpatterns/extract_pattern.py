import os
from typing import List

from bs4 import BeautifulSoup


def parsePatternHTML() -> List:
    file_dir = r"patterns_file"
    fileList = os.listdir(file_dir)
    patterns = []
    for file in fileList:
        pattern_list = getUserPatternlist(file_dir + "\\" + file)
        [patterns.append(record) for record in pattern_list]

    return patterns


def getUserPatternlist(file) -> List:
    pattern_list = []
    with open(file, 'r', encoding="utf-8") as f:
        htmldoc = f.read()
        soup = BeautifulSoup(htmldoc, "html.parser")
        g_lists = soup.find_all('div', class_="g_list")

        for n in range(len(g_lists)):
            lables = g_lists[n].find_all("div", class_="g_item")
            for i in range(len(lables)):
                g_item = lables[i]
                record = _get_record(g_item)
                pattern_list.append(record)

    return pattern_list


def _get_record(g_item) -> List:
    # 名称：
    pattern_name = g_item.find('li', class_="g_li").get("title")
    # 数据范畴：
    data_scope = g_item.find('li', class_="g_li1").get_text()
    # 状态：
    status = g_item.find('li', class_="g_li2").get_text()
    # 申请号、申请日
    apply_id = g_item.find('div', class_='g_cont').find('table') \
        .find_all('tr')[0].find('a').get_text()
    a = g_item.find('div', class_='g_cont').find('table') \
        .find_all('tr')[0].find_all('td')[1]
    [s.extract() for s in a("span")]
    apply_date = a.get_text(strip=True)

    # 公开(公告)号、公开(公告)日：
    a = g_item.find('div', class_='g_cont').find('table') \
        .find_all('tr')[1].find_all('td')
    [[s.extract() for s in item] for item in a]
    publish_id = a[0].get_text(strip=True)
    publish_date = a[1].get_text(strip=True)

    # 申请(专利权)人：
    a = g_item.find('div', class_='g_cont').find('table').find_all('tr')[3]
    [s.extract() for s in a("span")]
    applyer = a.get_text(strip=True)
    # 分类号
    a = g_item.find('div', class_='g_cont').find('table').find_all('tr')[4]
    [s.extract() for s in a("span")]
    classfication_id = a.get_text(strip=True)
    # 摘要
    abstract = g_item.find('div', class_='g_cont').find('table') \
        .find_all('tr')[6].find("span", style="font-weight:normal") \
        .get_text(strip=True)

    return [pattern_name, data_scope, status, apply_id, apply_date,
            publish_id, publish_date, applyer, classfication_id, abstract]


if __name__ == '__main__':
    file = 'patterns_file/example1.html'
    pattern_list = parsePatternHTML()
    from pandas.core.frame import DataFrame

    data = DataFrame(pattern_list)
    data.rename(columns={0: '专利名称', 1: '数据范畴', 2: '状态', 3: '申请号', 4: '申请日',
                         5: '公开(公告)号', 6: '公开(公告)日', 7: '申请(专利权)人', 8: '分类号',
                         9: '摘要'}, inplace=True)

    data.to_excel('patterns.xlsx', index=False)
