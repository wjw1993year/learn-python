"""
编程实现vlookup函数
输入：两个文件
1SRC_List.txt：一行是一个key，只有一列
2Map.txt：一行是一对key-value；
             多列时，第一列是key，其余列是value；只有一列时，key和value都是第一列
             列与列之间用\t隔开

输出：
OUTPUT_List.txt：第一列是SRC_List，第二列开始是对应的value
"""
from typing import List

SRC_LIST_PATH = "data/1SRC_List.txt"
MAP_PATH = "data/2Map.txt"
OUTPUT_PATH = "data/OUTPUT_List.txt"


def vlookup():
    src_list = _read_SRC_List(SRC_LIST_PATH)
    map = _read_Map(MAP_PATH)

    with open(OUTPUT_PATH, "w") as f:
        for key in src_list:
            value = map.get(key, "NULL")
            line = key + "\t" + value + "\n"
            f.write(line)


def _read_SRC_List(file_path: str) -> List:
    src_list = []
    with open(file_path, encoding='gbk') as f:
        for line in f:
            if line == '\n':
                continue
            src_list.append(line.strip())

    return src_list


def _read_Map(file_path: str) -> dict:
    map = {}
    with open(file_path, encoding='gbk') as f:
        for line in f:
            if line == '\n':
                continue
            elements = line.split("\t")
            key = elements[0]
            if len(elements) == 1:
                key = key.strip()
                value = key
            else:
                value = "\t".join(elements[1:])

            map[key] = value.strip()
    return map


if __name__ == '__main__':
    vlookup()
