"""
处理日志文件的分隔符
"""

import os

import pandas as pd
from tabulate import tabulate

from learnpy.workeasy.preprocesslog import Config


def _get_key_index(filename) -> str:
    base_name = os.path.basename(filename).lower()
    l = base_name.split(sep="_")
    key_idx = l[4]  # 日志文件类型
    return key_idx


def _get_columns(filename) -> dict:
    columns_map = {}
    idx = _get_key_index(filename)
    columns_list = Config.COLUMNS_NAME[idx]
    for i in range(len(columns_list)):
        columns_map[i] = columns_list[i]

    return columns_map


def _get_file_type(filename) -> dict:
    idx = _get_key_index(filename)
    return Config.FILE_TYPE[idx]


def _check_column(df, name, num_rows):
    req_col = []

    for col in df.columns:
        if name == '【Y】':
            if '【Y】' in col:
                req_col.append(col)
        else:
            if '【Y】' not in col and "预留字段" not in col:
                req_col.append(col)

    req_df = df[req_col]
    req_df = pd.DataFrame(
            req_df.isnull().sum()[
                req_df.isnull().sum() > 0].sort_index())
    req_df.rename(columns={0: "字段空值个数"}, inplace=True)
    req_df["字段空值率"] = req_df["字段空值个数"] / num_rows
    req_df.sort_values("字段空值个数", ascending=False, inplace=True)
    return req_df


def _check_and_print(df, file_name, sep):
    file_type_name = _get_file_type(file_name)
    columns_map = _get_columns(file_name)
    num_rows = df.shape[0]
    num_col = df.shape[1]
    print(f"- {os.path.basename(file_name)} 是{file_type_name}")
    sep_d = '0x01' if sep == '\x01' else sep
    print(f"- 列分隔符是 `{sep_d}`")

    if num_col == len(columns_map):
        df.rename(columns=columns_map, inplace=True)
        print(f"- 共 {num_col} 个字段（字段个数与采集规范一致），{num_rows} 条记录")
        print("- **必填字段，存在缺失值：**")
        req_df = _check_column(df, "【Y】", num_rows)
        req_df['字段空值率'] = req_df['字段空值率'].apply(lambda x: format(x, '.1%'))
        print(tabulate(req_df, headers=["字段名", "字段空值个数", "字段空值率（降序）"],
                       tablefmt='pipe'))
        print()
        print("- **选填字段（不包括预留字段），存在缺失值：**")
        nReq_df = _check_column(df, "【N】", num_rows)
        nReq_df['字段空值率'] = nReq_df['字段空值率'].apply(lambda x: format(x, '.1%'))
        print(tabulate(nReq_df, headers=["字段名", "字段空值个数", "字段空值率"],
                       tablefmt='pipe'))
    else:
        print(os.path.basename(file_name) + "的列数为：" + str(df.shape[1])
              + "，但正确的列数是" + str(len(columns_map)))
    print()
    print("========================================")
    print()


def to_excel(file_name, sep):
    data = pd.read_csv(file_name, sep=sep, header=None, low_memory=False)
    columns_map = _get_columns(file_name)
    # 稽核逻辑
    _check_and_print(data, file_name, sep)
    file_name = file_name.replace(".txt", "")
    data.to_excel(file_name + '.xlsx', index=False)


# 将file_path路径下所有txt文件转为excel
def parse_all(file_path, sep):
    fileList = os.listdir(file_path)
    for file_name in fileList:
        if ".txt" in file_name:
            to_excel(file_path + "\\" + file_name, sep)


if __name__ == '__main__':
    file_abs_path = r"D:\MyWork\3. 治理团队的工作\2. 具体工作\1. 各项目工作群\工号实名制疑难杂症沟通群\日志文件核对\日志数据\ygzyszh"
    sep = '\x01'
    # sep = '|'
    parse_all(file_abs_path, sep)
