"""
纵表变横表
"""
KEY_FIELD = 'key_field'
COLUMNS = "data/1key_columns.txt"
ZONG_TALBE = "data/2纵表.txt"
HENG_TABLE = "data/3横表.txt"


def zong2heng():
    cols = []
    col2idx = {}
    sort_list = []
    src_dict = {}
    with open(COLUMNS, encoding='utf8') as f:
        num_of_col = 0
        for line in f:
            if line == '\n':
                continue
            key = line.strip()
            cols.append(key)
            col2idx[key] = num_of_col
            num_of_col += 1
    if num_of_col == 0:
        raise RuntimeError

    with open(ZONG_TALBE, encoding='utf8') as f:
        for line in f:
            if line == '\n':
                continue
            element = line.strip().split("\t")
            key, value = element[0], element[1]

            if src_dict.get(key) is None:
                src_dict[key] = [0 for i in range(num_of_col)]
                sort_list.append(key)

            col_idx = col2idx[value]
            src_dict[key][col_idx] += 1

    with open(HENG_TABLE, mode='w') as f:
        header = KEY_FIELD + '\t' + '\t'.join(cols) + '\n'
        f.write(header)
        for key in sort_list:
            line = key + '\t' + '\t'.join(list(map(str, src_dict[key]))) + '\n'
            f.write(line)


if __name__ == '__main__':
    zong2heng()
