# coding:utf-8
"""
飞机航班的0-1整数规划问题
"""
import cvxpy as cxp
import numpy as np

# 输入
input_A = [
    [6, 8, 4, 4],
    [7, 7, 4, 4],
    [5, 8, 4, 4],
    [7, 9, 2, 5],
    [6, 7, 3, 4],
    [10, 6, 4, 3],
    [8, 8, 2, 3],
    [5, 11, 2, 4],
    [7, 7, 2, 5],
    [10, 4, 3, 6]
]

input_k = [[3, 3, 2, 2]]
input_b = [[6, 8, 4, 4]]


def solution():
    A = np.array(input_A)
    k = np.array(input_k)
    b = np.array(input_b)
    col_sum = [[1] for i in range(A.shape[0])]
    col_sum = np.array(col_sum)

    # 变量定义
    x = cxp.Variable(A.shape, integer=True)
    obj = cxp.Minimize(cxp.sum(cxp.abs(A - b + x)))

    con = [
        cxp.sum(x, keepdims=True, axis=1) == col_sum,
        cxp.sum(x, keepdims=True, axis=0) == k,
        x >= 0,
        x <= 1
    ]

    prob = cxp.Problem(obj, con)
    prob.solve(solver='GLPK_MI', verbose=True)
    print("最优值为：", prob.value)
    print("最优解为：\n", x.value)


if __name__ == '__main__':
    solution()
