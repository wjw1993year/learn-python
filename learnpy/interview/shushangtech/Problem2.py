# coding:utf-8
"""
波形模式匹配问题：
根据每行数字的波形图（也就是跳过每行开头的几个自然数，而只考虑那些介于-2和2之间的数），
编程统计其中有几种原型，每种原型有几个实际样式
"""

import os
from typing import List

import numpy as np

# csv文件的存放路径
FILE_PATH = r"data/test1_files/"

# 按照题目意思，一个pattern的长度范围是3~20
MIN_LEN = 3
MAX_LEN = 20
PATTERN_LEN_SCOPE = [i for i in range(MIN_LEN, MAX_LEN + 1)]


# 计算所有csv的最优解
def solution():
    data, fileList = _read_all_csv()
    f = open("Problem2_result.txt", "w")
    for idx in range(len(fileList)):
        if idx % 10 == 0:
            print(f"已完成文件数：{idx}个")

        row = data[idx]
        parsed_row, optimal_path = calculate(row)
        # 打印出原型和实际采样
        startIdx = 0
        freq = parsed_row[0]
        curve = parsed_row[1]
        path = optimal_path[1]
        patterns = []
        for i in range(len(freq)):
            k = freq[i]
            pattern_len = path[i]
            samples = []
            for j in range(k):
                samples.append(curve[startIdx:startIdx + pattern_len])
                startIdx += pattern_len
            samples = np.array(samples)
            pattern = np.mean(samples, axis=0, keepdims=True)
            patterns.append(pattern)

        print(f"{fileList[idx]}的总平方误差：{optimal_path[0]}", file=f)
        print("=================  " + fileList[idx] + "  =================",
              file=f)
        print(f"pattern出现次数及对应的长度：{[freq, path]}", file=f)
        print("各个pattern的具体值如下：", file=f)
        for pt in patterns:
            print(pt.tolist(), file=f)
        print("===========================================\n", file=f)

        # 绘制pattern曲线
        # pattern_curve = []
        # import matplotlib.pyplot as plt
        # for i in range(len(freq)):
        #     for j in range(freq[i]):
        #         item = patterns[i]
        #         for num in item[0].tolist():
        #             pattern_curve.append(num)
        #
        # plt.plot(curve, "b-*", pattern_curve, "r--o")
        # plt.show()
    f.close()


# 计算单个csv的最优解：深度优先搜索所有组合
def calculate(row: List):
    num_of_pattern = row[0]
    freq = row[1:1 + num_of_pattern]
    curve = row[1 + num_of_pattern:]
    parsed_row = [freq, curve]

    optimal_path = [np.inf, []]  # np.inf是路径的平方误差
    _dfs([], 0, freq, curve, optimal_path)
    return parsed_row, optimal_path


def _dfs(path, curLen, freq, curve, optimal_path):
    path_len = len(path)
    total_len = len(curve)
    if path_len == len(freq):
        if curLen == total_len:
            # 找到可行解，更新最优解
            err = _square_err_of(freq, path, curve)
            if err < optimal_path[0]:
                optimal_path[0] = err
                optimal_path[1] = [x for x in path]
        return

    # 回溯算法
    freq_k = freq[path_len]
    for pattern_len in PATTERN_LEN_SCOPE:
        pattern_total_len = freq_k * pattern_len
        act_remain_len = total_len - pattern_total_len - curLen
        min_remain_len = sum(freq[path_len + 1:]) * MIN_LEN
        max_remain_len = sum(freq[path_len + 1:]) * MAX_LEN
        # 选择的pattern_len过短，剪枝，但是继续找
        if act_remain_len > max_remain_len:
            continue
        # 选择的pattern_len过长，剪枝，但是不必继续
        if act_remain_len < min_remain_len:
            break
        # 找到了合法的pattern_len
        path.append(pattern_len)
        _dfs(path, curLen + pattern_total_len, freq, curve, optimal_path)
        # 回溯
        path.pop()


# 计算path的总平方误差
def _square_err_of(freq, path, curve) -> float:
    err = 0
    startIdx = 0
    for i in range(len(freq)):
        k = freq[i]
        pattern_len = path[i]
        samples = []
        for j in range(k):
            samples.append(curve[startIdx:startIdx + pattern_len])
            startIdx += pattern_len
        samples = np.array(samples)
        err += np.sum(np.var(samples, axis=0, keepdims=True))

    return err


# 将csv文件解析成数据
def _read_all_csv():
    data = []
    fileList = os.listdir(FILE_PATH)
    fileList.sort(key=lambda x: int(x.split(".")[0].replace("test", "")))
    for file_name in fileList:
        with open(FILE_PATH + file_name) as f:
            for line in f:
                row = []
                for num in line.strip().split(","):
                    if "." in num:
                        row.append(float(num))
                    else:
                        row.append(int(num))
            data.append(row)
        # # 单用例调试
        # break
    return data, fileList


if __name__ == '__main__':
    import time

    time_start_1 = time.time()
    # 主程序执行
    solution()
    time_end_1 = time.time()
    print("运行时间：" + str(time_end_1 - time_start_1) + "秒")
