# coding:utf-8
"""
题目1：上海收盘
"""

from functools import cmp_to_key

import numpy as np
import pandas as pd


# 每月的1号，先卖出股票，再买入股票
def transaction_at_day01(share_table, cash: float):
    my_shares = {}
    num_of_row = share_table.shape[1]
    last_month = '01'
    for idx in range(1, num_of_row):
        date = share_table.iloc[idx].name
        cur_month = date[5:7]
        # 月份发生了变化，故date就是cur_month的第一个交易日
        if cur_month != last_month:
            last_month = cur_month
            # 卖出
            cash += sale(my_shares, idx, share_table)
            # 买入
            my_shares = buy(cash, idx, share_table)
            cash = 0  # 买完后，cash清零
    # 返回：总资产=现金+股票
    return cash, my_shares


# 函数名：卖出股票
# 输入参数：
#   my_shares，持有的股票
#   idx，卖出日期
#   share_table：全量股票的价目表
# 输出：得到的现金
def sale(my_shares: dict, idx: int, share_table) -> float:
    cash = 0
    for key in my_shares:
        # 单价*数量
        cash += share_table.iloc[idx][key] * my_shares[key]
    return cash


# 函数名：买入股票
# 输入参数：
#   cash，现金
#   idx，买入日期
#   share_table：全量股票的价目表
# 输出：买入的股票（用字典表示，key是股票名，value是购买数量）
def buy(cash: float, idx: int, share_table) -> dict:
    # 1. 对全量股票按照上月的收益率进行排序
    growth_rate = []
    share_yesterday = share_table.iloc[idx - 1]  # 昨日的股票
    yesterday = share_yesterday.name
    for col_name in share_yesterday.index:
        price = share_yesterday[col_name]
        if not np.isnan(price):
            rate = _cal_growth_rate(col_name, yesterday, share_table)
            growth_rate.append((col_name, rate))
    sort_growth_rate = sorted(growth_rate, key=cmp_to_key(_cmp))

    # 2.排序完成后，选择收益率后10%的股票买入
    my_shares = {}
    share_today = share_table.iloc[idx]  # 昨日的股票
    num = len(growth_rate) // 10
    amount = cash / num
    for i in range(num):
        share_name = sort_growth_rate[i][0]
        value = amount / share_today[share_name]
        my_shares[share_name] = value

    return my_shares


# 计算当月收益率
def _cal_growth_rate(col_name: str, yesterday, share_table) -> float:
    day01 = yesterday[0:8] + "01"
    series = share_table.loc[day01:yesterday][col_name].dropna(how='all')
    # 特判，刚好在月底才上市，故增长率定位100%
    if len(series) == 1:
        return 1
    rate = (series[-1] - series[0]) / series[0]
    return rate


# 比较器
def _cmp(o1: tuple, o2: tuple) -> int:
    return o1[1] - o2[1]


if __name__ == '__main__':
    import time

    time_start_1 = time.time()

    share_table = pd.read_csv("data/shanghai.csv", index_col=[0])
    cash, my_shares = transaction_at_day01(share_table, 1000_000)
    # 计算总资产
    last_day_idx = share_table.shape[1] - 1
    cash += sale(my_shares, last_day_idx, share_table)
    print(cash)

    time_end_1 = time.time()
    print("运行时间：" + str(time_end_1 - time_start_1) + "秒")
