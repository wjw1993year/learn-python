"""
测试@staticmethod注解
"""


class StaticMethodTest(object):

    # 静态方法，类和对象都能调用
    @staticmethod
    def m1():
        print("static method")

    # 类方法，类和对象都能调用，但隐式地传了cls参数，cls代表的是类本身
    @classmethod
    def instance(cls):
        print('cls:', cls)
        return cls()


if __name__ == '__main__':
    StaticMethodTest.m1()
    obj = StaticMethodTest()
    obj.m1()
    obj2 = StaticMethodTest.instance()
    obj2.m1()
    obj2.instance()
