"""
测试魔法方法：__setattr__()
注意：在方法体内部，不能写self.key=value，否则会造成无限递归，导致栈溢出错误
"""
from collections import OrderedDict


class SetAttrTest(object):
    def __init__(self):
        self.ord = OrderedDict()

    def __setattr__(self, key, value):
        print((key, value))
        # self.key = value # 千万不能这样写，这样会造成无限递归调用，导致栈溢出
        # 应改为使用object的setattr方法来设置值，也可以改为用父类的super().__setattr__方法
        # object.__setattr__(self, key, value)
        if key == 'name':
            self.register_parameter(key, value)
        else:
            object.__setattr__(self, key, value)

    def register_parameter(self, key, value):
        self.ord[key] = value


if __name__ == '__main__':
    obj = SetAttrTest()
    obj.name = "wjw"
    print(obj.name)
