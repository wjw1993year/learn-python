"""
yield关键字，实现协程
"""


def consumer():
    r = ''
    while True:
        # n = yield r 等价于 yield r; n =yield
        n = yield r
        if not n:
            print("n is None")
            return
        print('[CONSUMER] Consuming %s...' % n)
        r = '200 OK'


def produce(c):
    # send(val)函数的执行步骤是：
    # 1先执行赋值，yield=val；
    # 2.再接着上次的位置继续执行，直到执行完yield，相当于next的效果
    c.send(None)
    n = 0
    while n < 5:
        n = n + 1
        print('[PRODUCER] Producing %s...' % n)
        r = c.send(n)
        print('[PRODUCER] Consumer return: %s' % r)
    c.close()


if __name__ == '__main__':
    c = consumer()
    produce(c)
