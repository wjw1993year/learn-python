"""
测试内置函数open
"""
from collections import Counter

if __name__ == '__main__':
    counter = Counter()
    with open("TestOpen.py", encoding="utf-8") as f:
        # python中，文件对象时可迭代的
        for line in f:
            counter.update(line.split())
            print(line, end="")

        # 效果和for line in f是一样的
        # while True:
        #     line = f.readline()
        #     if not line:
        #         break
        #     print(line, end="")

    print(counter)
