from typing import List
import numpy as np


# 操作类接口
class Operation(object):
    """
    操作：返回操作结果结点
    input_nodes: 输入结点列表
    name: 操作结果结点的名称
    """

    def __call__(self, input_nodes, name):
        pass

    """
    计算node结点的值
    node: 操作结果结点
    input_vals: 输入结点的值，与node.input_nodes是一一对应的
    """

    def compute(self, node, input_vals: List) -> np.ndarray:
        pass

    """
    计算node.input_nodes的jacobian向量积/梯度, dz/dX
    output_grad: dz/dY
    node：操作结果结点，即Y结点
    """

    def gradient(self, output_grad, node) -> List:
        pass


# 结点
class Node(object):

    # 构造器，主程序不使用这个构造器
    def __init__(self):
        self.input_nodes: List[Node] = []
        self.op: Operation = None
        self.name: str = ""

    def set_name(self, name: str):
        self.name = name

    def get_name(self) -> str:
        return self.name

    # 构造器，主程序只使用这个带参构造器
    @classmethod
    def instance(cls, input_nodes: List, op: Operation, name: str):
        node = cls()
        node.input_nodes = input_nodes
        node.op = op
        node.name = name
        return node

    """
    重写结点类的+-*/，以及右+-*/
    """

    # self + other: 重写为AddOp操作, 且右+与左+一致
    def __add__(self, other):
        name = ""
        if isinstance(other, Node):
            return add_op([self, other], name)
        else:
            const_node = Constant(other, str(other))
            return add_op([self, const_node], name)
    __radd__ = __add__

    # self - other: 重写为SubOp操作
    def __sub__(self, other):
        name = ""
        if isinstance(other, Node):
            return sub_op([self, other], name)
        else:
            const_node = Constant(other, str(other))
            return sub_op([self, const_node], name)

    def __rsub__(self, other):
        name = ""
        const_node = Constant(other, str(other))
        return sub_op([const_node, self], name)

    # self * other: 重写为MulOp操作，且右*与左*一致
    def __mul__(self, other):
        name = ""
        if isinstance(other, Node):
            return mul_op([self, other], name)
        else:
            const_node = Constant(other, str(other))
            return mul_op([self, const_node], name)
    __rmul__ = __mul__

    # self / other: 重写为DivOp操作
    def __truediv__(self, other):
        name = ""
        if isinstance(other, Node):
            return div_op([self, other], name)
        else:
            const_node = Constant(other, str(other))
            return div_op([self, const_node], name)

    def __rtruediv__(self, other):
        name = ""
        const_node = Constant(other, str(other))
        return div_op([const_node, self], name)

    # 相反数
    def __neg__(self):
        name = ""
        return neg_op([self], name)

    # 计算结点值
    def compute_val(self, input_vals: List) -> np.ndarray:
        return self.op.compute(self, input_vals)


# 操作类1：占位符操作类
class PlaceHolderOp(Operation):
    # 所有自变量结点都通过Variable()方法来生成
    def __call__(self, input_nodes, name) -> Node:
        node = Node.instance(input_nodes, self, name)
        return node

    # 占位符的值从node2ValMap中获取
    def compute(self, node: Node, input_vals: List):
        pass

    # 自变量结点无输入结点，故不存在输入结点梯度
    def gradient(self, output_grad, node):
        return None


# 产生自变量结点
def Variable(name: str) -> Node:
    node = place_holder_op([], name)
    return node


# 操作类2：常量操作类
class ConstOp(Operation):
    # 为保持方法签名的一致性，const不放在这个方法的入参中设置
    # 所有常量结点都通过Constant()方法来生成
    def __call__(self, input_nodes, name) -> Node:
        node = Node.instance([], self, name)
        return node

    def compute(self, node, input_vals: List):
        return node.const

    # 常量结点没有输入结点，故不存在输入结点的梯度
    def gradient(self, output_grad, node):
        return None


# 产生常量结点
def Constant(const, name: str = "") -> Node:
    node = const_op([], name)
    node.const = const
    return node


# 操作类3：element-wise加法，只支持常量的broadcast，因为常量的梯度不会被使用
class AddOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List) -> np.ndarray:
        return input_vals[0] + input_vals[1]

    # node.input_nodes中可能有常量，尽管output_grad与常量不同维度，
    # 但因为常量的梯度不会被使用，所有还是直接返回output_grad
    def gradient(self, output_grad: Node, node: Node) -> List[Node]:
        return [output_grad, output_grad]


# 操作类4：相反数
class NegOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List) -> np.ndarray:
        return -input_vals[0]

    def gradient(self, output_grad, node) -> List[Node]:
        return [-output_grad]


# 操作类5：element-wise减法
class SubOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List):
        return input_vals[0] - input_vals[1]

    def gradient(self, output_grad, node) -> List[Node]:
        return [output_grad, -output_grad]


# 操作类6：element-wise乘法
class MulOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List) -> np.ndarray:
        return input_vals[0] * input_vals[1]

    def gradient(self, output_grad, node) -> List[Node]:
        node0 = node.input_nodes[0]
        node1 = node.input_nodes[1]
        return [output_grad * node1, output_grad * node0]


# 操作类7：element-wise除法
class DivOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List):
        return input_vals[0] / input_vals[1]

    def gradient(self, output_grad, node) -> List[Node]:
        node0 = node.input_nodes[0]
        node1 = node.input_nodes[1]
        return [output_grad / node1, -output_grad * node0 / (node1 * node1)]


# 操作类8：矩阵转置
class MatTransposeOp(Operation):

    def __call__(self, input_nodes, name=""):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List[np.ndarray]) -> np.ndarray:
        return np.transpose(input_vals[0])

    def gradient(self, output_grad: Node, node) -> List:
        return [transpose_op([output_grad])]


# 操作类9：矩阵乘
class MatMulOp(Operation):

    def __call__(self, input_nodes, name):
        node = Node.instance(input_nodes, self, name)
        return node

    def compute(self, node, input_vals: List) -> np.ndarray:
        mat_a = input_vals[0]
        mat_b = input_vals[1]
        return np.matmul(mat_a, mat_b)

    def gradient(self, output_grad, node) -> List:
        mat_a = node.input_nodes[0]
        mat_b = node.input_nodes[1]
        partial_a = output_grad * transpose_op(mat_b)
        partial_b = transpose_op(mat_a) * output_grad
        return [partial_a, partial_b]


# 操作类10：element-wise log
class LogOp(Operation):

    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    def compute(self, node, input_vals: List) -> np.ndarray:
        return np.log(input_vals[0])

    def gradient(self, output_grad, node) -> List:
        node0 = node.input_nodes[0]
        return [output_grad / node0]


def log(node: Node, name="") -> Node:
    return log_op([node], name)


# 操作类11：element-wise exp
class ExpOp(Operation):

    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    def compute(self, node, input_vals: List) -> np.ndarray:
        return np.exp(input_vals[0])

    def gradient(self, output_grad, node) -> List:
        node0 = node.input_nodes[0]
        return [output_grad * exp_op([node0], "")]


def exp(node: Node, name="") -> Node:
    return exp_op([node], name)


# 操作类12：同维零矩阵
class ZeroLikeOp(Operation):

    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    def compute(self, node, input_vals: List) -> np.ndarray:
        return np.zeros_like(input_vals[0])

    def gradient(self, output_grad, node) -> List:
        node0 = node.input_nodes[0]
        return [zero_like_op([node0], "")]


# 操作类13：同维1矩阵
class OneLikeOp(Operation):

    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    def compute(self, node, input_vals: List) -> np.ndarray:
        return np.ones_like(input_vals[0])

    def gradient(self, output_grad, node) -> List:
        node0 = node.input_nodes[0]
        return [zero_like_op([node0], "")]


# 操作类14：降维求和 reduce_sum, 输出总和
class ReduceSum(Operation):

    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    def compute(self, node, input_vals: List) -> np.ndarray:
        return np.sum(input_vals[0])

    def gradient(self, output_grad, node) -> List:
        node0 = node.input_nodes[0]
        return [output_grad * one_like_op([node0], "")]


def reduce_sum(node: Node, name="") -> Node:
    return reduce_sum_op([node], name)


# 操作类15：Jacobian向量积
# 上述的操作1~14的gradient()方法没有用到这个操作，因为它们用矩阵的运算就能计算梯度
# 但如果新增的操作类无法用矩阵简单地计算梯度时，就需要用到Jacobian向量积操作来计算梯度
class JacobianOp(Operation):

    # input_nodes[0]代表partial_z_to_Y
    # input_nodes[1:]代表partial_yj_to_X
    def __call__(self, input_nodes, name):
        return Node.instance(input_nodes, self, name)

    # partial_z_to_Y已被排成一个行向量，Yj排序方法是按照维度排序:
    # 假设Y的维度是(a,b,c), 则j的范围是[0, a*b*c)
    # partial_Y_to_X与Yj一一对应
    def compute(self, node, input_vals: List) -> np.ndarray:
        partial_z_to_Y = input_vals[0]
        partial_Y_to_X = input_vals[1:]
        n = len(partial_z_to_Y)
        s = np.array(0)
        for j in range(n):
            s += partial_z_to_Y[j] * partial_Y_to_X[j]
        return s

    # 不需要用到
    def gradient(self, output_grad, node) -> List:
        pass


# 各个操作类的实例
place_holder_op = PlaceHolderOp()
const_op = ConstOp()
add_op = AddOp()
neg_op = NegOp()
sub_op = SubOp()
mul_op = MulOp()
div_op = DivOp()
transpose_op = MatTransposeOp()
mat_mul_op = MatMulOp()
log_op = LogOp()
exp_op = ExpOp()
zero_like_op = ZeroLikeOp()
one_like_op = OneLikeOp()
reduce_sum_op = ReduceSum()
jacobian_op = JacobianOp()


# BP自动微分
def bp_diff(z: Node, node_list: List[Node]) -> List[Node]:
    dp = {z: Constant(1)}  # 记录表
    grad_node_list = []
    # 由于结点的属性记录了父结点input_nodes，没有记录它的子节点
    # 所以要先遍历图，把所有结点的子节点都找出来，并存储到map中
    node_to_son_map = find_son(z)
    for x in node_list:
        grad_x = build_grad(dp, node_to_son_map, x)
        grad_node_list.append(grad_x)
    return grad_node_list


def build_grad(dp: dict, node_to_son_map: dict, x: Node) -> Node:
    if x in dp:
        return dp[x]
    sons = node_to_son_map[x]
    if sons is None:
        # 与z结点无关的结点才会走入这个分支
        dp[x] = zero_like_op(x, "")
        return dp[x]

    jacobian_list = []
    for y in sons:
        # 计算每个子节点的jacobian向量积
        y_grad = build_grad(dp, node_to_son_map, y)
        # 以下两行可以优化为用map来存储jacobian_of_inputs，只计算一次jacobian_of_inputs
        jacobian_of_inputs = y.op.gradient(y_grad, y)
        x_idx = y.input_nodes.index(x)

        jacobian = jacobian_of_inputs[x_idx]
        jacobian_list.append(jacobian)
    # 将所有子结点的jacobian向量积加起来就是x结点的梯度
    dp[x] = sum_jacobian(jacobian_list)
    return dp[x]


# 各个结点的儿子结点
def find_son(root: Node) -> dict:
    son_map = dict()
    son_map[root] = []
    visited = set()
    dfs_find_son(root, visited, son_map)
    return son_map


def dfs_find_son(root: Node, visited: set, son_map: dict):
    if root in visited:
        return
    # 前序遍历
    visited.add(root)
    for parent in root.input_nodes:
        if parent not in son_map:
            son_map[parent] = [root]
        else:
            son_map[parent].append(root)
        dfs_find_son(parent, visited, son_map)


# 返回node_list的和结点
def sum_jacobian(node_list: List[Node]) -> Node:
    from operator import add
    from functools import reduce
    return reduce(add, node_list)


# 自动微分，迭代写法
def bp_diff_iter(z: Node, node_list: List[Node]) -> List[Node]:
    topo_order = reversed(find_topo_sort([z]))
    grad_table = {}  # 梯度表
    node_to_jacobians = {z: [Constant(1)]}  # 雅可比向量积
    for y in topo_order:
        grad_y = sum_jacobian(node_to_jacobians[y])
        grad_table[y] = grad_y
        # y的各个输入结点的jacobian向量积，与y。input_nodes是一一对应的
        jacobian_of_inputs = y.op.gradient(grad_y, y)
        for i in range(len(y.input_nodes)):
            x = y.input_nodes[i]
            grad_x = jacobian_of_inputs[i]
            if x not in node_to_jacobians:
                node_to_jacobians[x] = [grad_x]
            else:
                node_to_jacobians[x].append(grad_x)

    grad_node_list = [grad_table[node] for node in node_list]
    return grad_node_list


# 拓扑排序
def find_topo_sort(node_list: List[Node]) -> List[Node]:
    visited = set()
    topo_order = []
    for node in node_list:
        post_dfs(node, visited, topo_order)
    return topo_order


# 后序遍历获得拓扑排序
def post_dfs(root: Node, visited: set, topo_order: List[Node]):
    if root in visited:
        return
    visited.add(root)
    if isinstance(root.input_nodes, Node):
        print(1)

    for node in root.input_nodes:
        post_dfs(node, visited, topo_order)
    topo_order.append(root)


# 图计算引擎/执行器
class Executor:

    """
    初始化
    eval_node_list: 要计算的结点
    """
    def __init__(self, eval_node_list: List[Node] = None):
        if eval_node_list is None:
            eval_node_list = []
        self.eval_node_list = eval_node_list

    """
    执行一次计算图，把eval_node_list中的结点及其祖先结点的值计算出来
    feed_dict: 输入自变量结点的值
    """
    def run(self, feed_dict: dict) -> List[np.ndarray]:
        # 按照拓扑排序的顺序去求值，可以保证计算当前结点的值时，它的输入结点的值都是可用的
        topo_order = find_topo_sort(self.eval_node_list)

        # 初始化node2valMap，填充自变量结点的值
        node_2_val_map = dict(feed_dict)
        for node in topo_order:
            if node in node_2_val_map:
                # 算过就不必再算一次
                continue
            input_vals = [node_2_val_map[x] for x in node.input_nodes]
            node_2_val_map[node] = node.compute_val(input_vals)

        result = [node_2_val_map[x] for x in self.eval_node_list]
        return result

